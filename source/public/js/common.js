/*
 |--------------------------------------------------------------------------
 | Common custom Javascript
 |--------------------------------------------------------------------------
 |
 | Maybe use Mix in the future:
 | https://laravel.com/docs/7.x/frontend#writing-javascript
 | https://laravel.com/docs/7.x/mix
 |  Create the file in resources/js (called 'file.js' in this example)
 |  Go to webpack.mix.js file (at the bottom)
 |  Add there: mix.js('resources/js/file.js', 'public/js');
 |  Run in terminal : npm run production
 |
 |
 */

/**
 * Enum for nodable type
 * @type {Readonly<{MOVIE: string, SERIES: string, VIDEO: string, NOTE: string, BOOK: string, VIDEO_GAME: string}>}
 */
const Nodable = Object.freeze({
    NOTE:   "Note",
    BOOK:   "Book",
    MOVIE:  "Movie",
    MUSIC:  "Music",
    PERSON: "Person",
    SERIES: "Series",
    VIDEO:  "Video",
    VIDEO_GAME: "VideoGame"
});

/**
 * Displays a message in the dedicated message DIV
 * @param message
 */
function displayMessage(message, level='warning') {
    // Removes final dot
    message = message.replace(/\.$/, "");
    // TODO: style the message better
    let messageDiv = document.getElementById('message');
    messageDiv.innerHTML = message;
    messageDiv.style.display = 'block';
    switch (level) {
        case 'info':
            messageDiv.classList.value = "message info";
            console.info(message);
            break;
        case 'warning':
            messageDiv.classList.value = "message warning";
            console.warn(message);
            break;
        case 'error':
            messageDiv.classList.value = "message error";
            console.error(message);
            break;
        default:
            console.log(message);
    }

}

/**
 * Hides the DIV dedicated to messages
 * @param message
 */
function hideMessage() {
    document.getElementById("message").style.display = "none";
}

/**
 * Opens a XMLHttpRequest with necessary headers
 * @param method PUT, POST, GET... in capital letters
 * @param url
 * @returns {XMLHttpRequest}
 */
function getXMLHttpRequest(method, url, json=false) {
    let xhr = new XMLHttpRequest();
    xhr.open(method, url);
    let csrf = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
    xhr.setRequestHeader('X-CSRF-TOKEN', csrf);
    if (method === 'PUT' || method === 'POST') {
        if (json) {
            xhr.setRequestHeader('Content-Type', 'application/json');
        } else {
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        }
    }
    return xhr;
}

/**
 * Opens the given URL in the same window
 * @param url
 */
function openPage(url) {
    window.open(url, '_self');
}

/**
 * Opens the given URL in a new window
 * @param url
 */
function openNewPage(url) {
    window.open(url, '_blank');
}

/**
 * Removes all line break / new line / EOL
 * @param string
 * @returns string
 */
function removeLineBreaks(string) {
    return string.replace(/(\r\n|\n|\r)/gm,"");
}

/**
 * Gets the error message from a XHR response.
 */
function getXhrErrorMessage(xhr) {
    // Should we care about xhr.status?
    try {
        return JSON.parse(xhr.responseText).errorMessage;
    } catch (err) {
        return xhr.responseText;
    }
}

/**
 * Returns the value of the specified element
 * @param elementId
 * @returns {*}
 */
function valueOf(elementId) {
    return document.getElementById(elementId).value;
}

/**
 * Gets the value of an element and apply encodeURIComponent to it.
 * Commonly used to get data and use them in an HTTP request.
 * @param elementId
 * @returns {string}
 */
function urlEncodeInputValue(elementId) {
    return encodeURIComponent(valueOf(elementId));
}

/**
 * Stops propagation of clicks on underneath div etc...
 * @param e the Event
 */
function stopPropagation()
{
    if (typeof event !== 'undefined') {
        event.stopPropagation();
    }

    /*
    if (!e) e = window.event;

    //IE9 & Other Browsers
    if (e.stopPropagation) {
        e.stopPropagation();
    }
    //IE8 and Lower
    else {
        e.cancelBubble = true;
    }*/
}

/**
 * Selects elements by class name(s), and for each of them set the desired display ("", "none", "block" etc...)
 * @param className
 * @param display ("", "none", "block" etc...)
 */
function setDisplayByClassName(className, display) {
    let elements = document.getElementsByClassName(className);
    for (let i = 0; i < elements.length; ++i) {
        elements[i].style.display = display;
    }
}

/**
 * Changes display for an element
 * @param elementId
 * @param display ("", "none", "block" etc...)
 */
function setDisplayById(elementId, display) {
    let element = document.getElementById(elementId);
    if (element === null) {
        console.error('Error setting display for elementId "' + elementId + '": not found.');
    } else {
        element.style.display = display;
    }
}