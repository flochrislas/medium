/*********************************************************************************************************************
 *                                                                                                                   *
 *     JS for the website GROUPS list                                                                                *
 *                                                                                                                   *
 *********************************************************************************************************************/


/*********************************************************************************************************************/
/* Init actions                                                                                                      */
/*********************************************************************************************************************/

/*********************************************************************************************************************/
/* Events                                                                                                            */
/*********************************************************************************************************************/

function setWebsiteGroupsEditEventListeners(id) {
    document.getElementById("websiteGroupTitle"+id)
        .addEventListener("keydown",
            (event) => updateWebsiteGroupOnKeypress(event, id));
}

function updateWebsiteGroupOnKeypress(event, id) {
    // If Enter key is pressed (event.keyCode 13)
    if (event.key === "Enter") {
        // Ensure it is only this code that runs
        event.preventDefault();
        // Update
        updateWebsiteGroup(id);
    }
    // If Escape key is pressed (event.keyCode 27)
    if (event.key === "Escape") {
        // Ensure it is only this code that runs
        event.preventDefault();
        // "refresh" TODO: reload group list from DB
        updateWebsiteGroup(id);
    }
}

/*********************************************************************************************************************/
/* DOM actions                                                                                                       */
/*********************************************************************************************************************/

function generateWebsiteGroupsHtml(websiteGroups) {
    if (typeof websiteGroups === 'undefined') return "";
    let html = '';
    for (let i = 0; i < websiteGroups.length; ++i) {
        html += '<div id="websiteGroupRow'+websiteGroups[i].id+'" class="websiteGroupRow">\n' +
            '           <div id="websiteGroupEditButton'+websiteGroups[i].id+'" class="websiteGroupEditButton" onclick="editWebsiteGroup('+websiteGroups[i].id+')">'+SvgIcons.edit()+'</div>\n';
        if (i!==0)
            html +='            <div class="websiteGroupMove" onclick="moveWebsiteGroup('+websiteGroups[i].id+', '+(websiteGroups[i].position-1)+')">'+SvgIcons.swapUp()+'</div>\n' ;
        if (i!==websiteGroups.length-1)
            html +='            <div class="websiteGroupMove" onclick="moveWebsiteGroup('+websiteGroups[i].id+', '+(websiteGroups[i].position+1)+')">'+SvgIcons.swapDown()+'</div>\n' ;
        html +='            <div id="websiteGroupTitle'+websiteGroups[i].id+'" class="websiteGroupTitle" contenteditable="false" onclick="websiteShowFromGroup('+websiteGroups[i].id+')">'+websiteGroups[i].title+'</div>\n' +
            '            <div id="websiteGroupUpdateButton'+websiteGroups[i].id+'" class="websiteGroupUpdateButton" onclick="updateWebsiteGroup('+websiteGroups[i].id+')">'+SvgIcons.save()+'</div>\n' +
            '            <div id="websiteGroupRemoveButton'+websiteGroups[i].id+'" class="websiteGroupRemoveButton" onclick="destroyWebsiteGroup('+websiteGroups[i].id+')">'+SvgIcons.trash()+'</div>\n' +
            '    </div>';
    }
    document.getElementById("websiteGroupListContent").innerHTML = html;
    setWebsiteGroupSwapVisibility();
}

function editWebsiteGroup(id) {
    document.getElementById("websiteGroupRemoveButton"+id).style.display = "inline-block";
    document.getElementById("websiteGroupUpdateButton"+id).style.display = "inline-block";
    document.getElementById("websiteGroupRow"+id).classList.remove('websiteGroupRow');
    document.getElementById("websiteGroupRow"+id).classList.add('websiteGroupRowEditing');

    // Makes text editable
    document.getElementById("websiteGroupTitle"+id).setAttribute("contenteditable", "true");
    // Places the cursor at the end of the editable text
    //document.getElementById("websiteGroupTitle"+id).focus();
    // Selects all the editable text
    window.getSelection().selectAllChildren(document.getElementById("websiteGroupTitle"+id));
    setWebsiteGroupsEditEventListeners(id);
}

function filterWebsiteGroups() {
    let input, filter, rows, i, title, txtValue;
    input = document.getElementById('websiteGroupSearch');
    filter = input.value.toUpperCase();
    rows = document.getElementsByClassName('websiteGroupRow');

    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < rows.length; ++i) {
        title = rows[i].getElementsByClassName('websiteGroupTitle')[0];
        txtValue = title.textContent || title.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            rows[i].style.display = "";
        } else {
            rows[i].style.display = "none";
        }
    }
}

/*********************************************************************************************************************/
/* Form data handling                                                                                                */
/*********************************************************************************************************************/


/*********************************************************************************************************************/
/* Call-back functions                                                                                               */
/*********************************************************************************************************************/

function refreshWebsiteGroupFromResponse(xhr) {
    if (xhr.status === 200 || xhr.status === 201) {
        let response = JSON.parse(xhr.responseText);
        generateWebsiteGroupsHtml(response.websiteGroups);
        if (xhr.status === 201) {
            editWebsiteGroup(response.createdId);
        }
    } else if (xhr.status === 204) {
        // do nothing ^^b
    } else {
        displayMessage(xhr.responseText);
    }
}

/*********************************************************************************************************************/
/* AJAX calls                                                                                                        */
/*********************************************************************************************************************/

function storeWebsiteGroup(title) {
    let url = WebsiteGroupRoutes.create();
    let xhr = getXMLHttpRequest('POST', url);
    xhr.onload = function() {
        refreshWebsiteGroupFromResponse(xhr);
    };
    xhr.send(encodeURI('title=' + title));
}

function updateWebsiteGroup(id) {
    // Deals with UI
    // hide delete button
    document.getElementById("websiteGroupRemoveButton"+id).style.display = "none";
    document.getElementById("websiteGroupUpdateButton"+id).style.display = "none";
    document.getElementById("websiteGroupRow"+id).classList.remove('websiteGroupRowEditing');
    document.getElementById("websiteGroupRow"+id).classList.add('websiteGroupRow');
    // Actual update
    let title = document.getElementById("websiteGroupTitle"+id).innerText;
    let url = WebsiteGroupRoutes.update(getActiveWebsiteGroupId());
    let xhr = getXMLHttpRequest('PUT', url);
    xhr.onload = function() {
        document.getElementById("websiteGroupTitle"+id).setAttribute("contenteditable", "false");
        refreshWebsiteGroupFromResponse(xhr);
    };
    xhr.send(encodeURI('title=' + title));
}

function destroyWebsiteGroup(id) {
    let url = WebsiteGroupRoutes.delete(id);
    let xhr = getXMLHttpRequest('DELETE', url);
    xhr.onload = function() {
        refreshWebsiteGroupFromResponse(xhr);
    };
    xhr.send(null);
}

function moveWebsiteGroup(id, position) {
    let url = WebsiteGroupRoutes.position();
    let xhr = getXMLHttpRequest('POST', url);
    xhr.onload = function() {
        refreshWebsiteGroupFromResponse(xhr);
    };
    xhr.send(encodeURI('groupId=' + id + '&position=' + position));
}

/*********************************************************************************************************************/
/* Getters and setters                                                                                               */
/*********************************************************************************************************************/

function isWebsiteGroupSwapVisible() {
    let alreadyVisible = document.getElementById("showWebsiteGroupSwap").value;
    return alreadyVisible === "1";
}

/*********************************************************************************************************************/
/* DOM element display                                                                                               */
/*********************************************************************************************************************/

function toggleWebsiteGroupSwap() {
    let moveButtons = document.getElementsByClassName("websiteGroupMove");
    let alreadyVisible = isWebsiteGroupSwapVisible();
    let i;
    for (i=0; i < moveButtons.length; ++i) {
        if (!alreadyVisible) {
            moveButtons[i].style.display = "inline-block";
        } else {
            moveButtons[i].style.display = "none";
        }
    }
    if (alreadyVisible) {
        document.getElementById("showWebsiteGroupSwap").value = "0";
    } else {
        document.getElementById("showWebsiteGroupSwap").value = "1";
    }
}

function setWebsiteGroupSwapVisibility() {
    let moveButtons = document.getElementsByClassName("websiteGroupMove");
    let i;
    if (isWebsiteGroupSwapVisible()) {
        for (i=0; i < moveButtons.length; ++i) {
            moveButtons[i].style.display = "inline-block";
        }
    } else {
        for (i=0; i < moveButtons.length; ++i) {
            moveButtons[i].style.display = "none";
        }
    }
}

function toggleWebsiteGroupSearch() {
    // Hide swap icons
    if (isWebsiteGroupSwapVisible()) {
        document.getElementById("showWebsiteGroupSwap").value = "0";
        setWebsiteGroupSwapVisibility();
    }
    // Hide header icons
    document.getElementById("websiteGroupToggleMoveButton").style.display = "none";
    document.getElementById("websiteGroupSearchButton").style.display = "none";
    document.getElementById("websiteGroupHeader").style.display = "none";
    document.getElementById("websiteGroupAddButton").style.display = "none";
    // Show filter inputs
    document.getElementById("websiteGroupSearch").style.display = "inline-block";
    document.getElementById("websiteGroupSearchClose").style.display = "inline-block";
    // Focus on search text box
    document.getElementById("websiteGroupSearch").focus();
    document.getElementById("websiteGroupSearch").select();
}

function closeWebsiteGroupSearch() {
    // Show header icons
    document.getElementById("websiteGroupToggleMoveButton").style.display = "inline-block";
    document.getElementById("websiteGroupSearchButton").style.display = "inline-block";
    document.getElementById("websiteGroupHeader").style.display = "inline-block";
    document.getElementById("websiteGroupAddButton").style.display = "inline-block";
    // Hide filter inputs
    document.getElementById("websiteGroupSearch").style.display = "none";
    document.getElementById("websiteGroupSearchClose").style.display = "none";
    // Make all rows visible
    let rows = document.getElementsByClassName('websiteGroupRow');
    for (let i = 0; i < rows.length; ++i) {
        rows[i].style.display = "";
    }
}