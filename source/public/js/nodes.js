/********************************************************************************************************************
*                                                                                                                   *
*     JS for the node details element                                                                               *
*                                                                                                                   *
*********************************************************************************************************************/


/*********************************************************************************************************************/
/* Init actions                                                                                                      */
/*********************************************************************************************************************/

if (!loadNode(getLoadedNodeId())) {
    setDefaultValues();
}
setNodeEditEventListeners();

/*********************************************************************************************************************/
/* Events                                                                                                            */
/*********************************************************************************************************************/

function setNodeEditEventListeners() {
    document.getElementsByName('nodeInput').forEach(item => {
        item.addEventListener('keydown', event => {
            // Saves upon Ctrl+S
            if((event.ctrlKey || event.metaKey) && event.key === 's') {
                event.preventDefault()
                if (!getNodeChangesAreSaved()) {
                    saveNode();
                }
            } else {
                // Shows unsaved changes upon input modification
                if (getNodeChangesAreSaved() && !event.ctrlKey && !event.metaKey) {
                    showNodeSaveUnsaved();
                    setNodeChangesAreSaved('false');
                }
            }
        })
    })
    /* let elements = document.querySelectorAll('input[id^="node"]'); */
}

/*********************************************************************************************************************/
/* DOM actions                                                                                                       */
/*********************************************************************************************************************/

function newNode() {
    setLoadedNodeId("");
    setNodableType("");
    clearAllNodeInputs();
    setNodeChangesAreSaved('true');
    hideNodeDelete();
    document.getElementById("nodeElement").style.visibility = "visible";
    setDefaultValues();
}

function saveNode() {
    let data = buildJsonData();
    let nodeId = getLoadedNodeId();
    if (nodeId === "") {
        createNode(data);
    } else {
        updateNode(data);
    }
}

function setNodableType(nodableType) {
    // Stores info
    document.getElementById("nodeNodableType").value = nodableType;
    // Clears nodable div display
    setDisplayByClassName("nodable", "none");

    switch(nodableType) {
        case Nodable.NOTE:
            document.getElementById("nodeNodableType").value = "";
            break;
        case Nodable.PERSON:
            // Set to private by default
            document.getElementById("nodePrivacy").value = "2";
            // no break;
        default:
            if (nodableType !== '') {
                // Displays nodable div
                let elementId = "nodable" + nodableType;
                setDisplayById(elementId, "block");
                if (document.getElementById("nodeTopic").value === "Thought") {
                    document.getElementById("nodeTopic").value = nodableType;
                }
            }
    }
}

function setDefaultValues() {
    // Privacy: public
    document.getElementById("nodePrivacy").value = "0";
    // Default to quick note
    document.getElementById("nodeMedia").value = "Note";
    document.getElementById("nodeTopic").value = "Thought";
    document.getElementById("nodeTitle").value = "Quick note";
}

/*********************************************************************************************************************/
/* Form data handling                                                                                                */
/*********************************************************************************************************************/

function showNode(node) {
    setLoadedNodeId(node.id);
    setNodableType(node.nodable_type);
    clearAllNodeInputs();
    setNodeChangesAreSaved('true');
    document.getElementById("nodeMedia").value = node.media;
    document.getElementById("nodeTopic").value = node.topic;
    document.getElementById("nodePrivacy").value = node.privacy;
    document.getElementById("nodeNeedMore").value = node.needMore;
    document.getElementById("nodeTitle").value = node.title;
    document.getElementById("nodeContent").value = node.content;
    document.getElementById("nodeComment").value = node.comment;
    document.getElementById("nodeTags").value = node.tags;

    switch(node.nodable_type) {
        case Nodable.BOOK:
            document.getElementById("bookTitle").value = node.nodable.title;
            document.getElementById("bookCreator").value = node.nodable.creator;
            document.getElementById("bookReleaseDate").value = node.nodable.release_date;
            document.getElementById("bookStatus").value = node.nodable.status;
            document.getElementById("bookGrade").value = node.nodable.grade;
            document.getElementById("bookScore").value = node.nodable.score;
            break;

        case Nodable.MOVIE:
            document.getElementById("movieTitle").value = node.nodable.title;
            document.getElementById("movieCreator").value = node.nodable.creator;
            document.getElementById("movieReleaseDate").value = node.nodable.release_date;
            document.getElementById("movieStatus").value = node.nodable.status;
            document.getElementById("movieGrade").value = node.nodable.grade;
            document.getElementById("movieScore").value = node.nodable.score;
            break;

        case Nodable.MUSIC:
            document.getElementById("musicCreator").value = node.nodable.creator;
            document.getElementById("musicReleaseDate").value = node.nodable.release_date;
            document.getElementById("musicStatus").value = node.nodable.status;
            document.getElementById("musicGrade").value = node.nodable.grade;
            document.getElementById("musicScore").value = node.nodable.score;
            break;

        case Nodable.PERSON:
            document.getElementById("personName").value = node.nodable.name;
            document.getElementById("personContact").value = node.nodable.contact;
            document.getElementById("personBirthDate").value = node.nodable.birth_date;
            break;

        case Nodable.SERIES:
            document.getElementById("seriesTitle").value = node.nodable.title;
            document.getElementById("seriesReleaseDate").value = node.nodable.release_date;
            document.getElementById("seriesStatus").value = node.nodable.status;
            document.getElementById("seriesGrade").value = node.nodable.grade;
            document.getElementById("seriesScore").value = node.nodable.score;
            break;

        case Nodable.VIDEO:
            document.getElementById("videoTitle").value = node.nodable.title;
            document.getElementById("videoCreator").value = node.nodable.creator;
            document.getElementById("videoReleaseDate").value = node.nodable.release_date;
            document.getElementById("videoStatus").value = node.nodable.status;
            document.getElementById("videoGrade").value = node.nodable.grade;
            document.getElementById("videoScore").value = node.nodable.score;
            break;

        case Nodable.VIDEO_GAME:
            document.getElementById("videoGameTitle").value = node.nodable.title;
            document.getElementById("videoGameCreator").value = node.nodable.creator;
            document.getElementById("videoGameReleaseDate").value = node.nodable.release_date;
            document.getElementById("videoGameStatus").value = node.nodable.status;
            document.getElementById("videoGameGrade").value = node.nodable.grade;
            document.getElementById("videoGameScore").value = node.nodable.score;
            break;
    }

    // In case the node element was hidden after a node got deleted, we need to do this:
    showNodeDelete();
    showNodeSaveNeutral();
    document.getElementById("nodeElement").style.visibility = "visible";
}

function clearAllNodeInputs() {
    let elements = document.getElementsByName("nodeInput");
    for (let i = 0; i < elements.length; ++i) {
        elements[i].value = "";
    }
}

function buildJsonData() {
    let nodeData = {};
    nodeData.title = valueOf("nodeTitle");
    nodeData.content = valueOf("nodeContent");
    nodeData.comment = valueOf("nodeComment");
    nodeData.tags = valueOf("nodeTags");
    nodeData.media = valueOf("nodeMedia");
    nodeData.topic = valueOf("nodeTopic");
    nodeData.privacy = valueOf("nodePrivacy");
    nodeData.needMore = valueOf("nodeNeedMore");
    nodeData.nodable_type = valueOf("nodeNodableType");
    let nodableData = {};
    switch(valueOf("nodeNodableType")) {
        case Nodable.BOOK:
            nodableData.title = valueOf("bookTitle");
            nodableData.creator = valueOf("bookCreator");
            nodableData.release_date = valueOf("bookReleaseDate");
            nodableData.status = valueOf("bookStatus");
            nodableData.grade = valueOf("bookGrade");
            nodableData.score = valueOf("bookScore");
            break;
        case Nodable.MOVIE:
            nodableData.title = valueOf("movieTitle");
            nodableData.creator = valueOf("movieCreator");
            nodableData.release_date = valueOf("movieReleaseDate");
            nodableData.status = valueOf("movieStatus");
            nodableData.grade = valueOf("movieGrade");
            nodableData.score = valueOf("movieScore");
            break;
        case Nodable.MUSIC:
            nodableData.creator = valueOf("musicCreator");
            nodableData.release_date = valueOf("musicReleaseDate");
            nodableData.status = valueOf("musicStatus");
            nodableData.grade = valueOf("musicGrade");
            nodableData.score = valueOf("musicScore");
            break;
        case Nodable.PERSON:
            nodableData.personName = valueOf("personName");
            nodableData.personContact = valueOf("personContact");
            nodableData.personBirthDate = valueOf("personBirthDate");
            break;
        case Nodable.SERIES:
            nodableData.title = valueOf("seriesTitle");
            nodableData.release_date = valueOf("seriesReleaseDate");
            nodableData.status = valueOf("seriesStatus");
            nodableData.grade = valueOf("seriesGrade");
            nodableData.score = valueOf("seriesScore");
            break;
        case Nodable.VIDEO:
            nodableData.title = valueOf("videoTitle");
            nodableData.creator = valueOf("videoCreator");
            nodableData.release_date = valueOf("videoReleaseDate");
            nodableData.status = valueOf("videoStatus");
            nodableData.grade = valueOf("videoGrade");
            nodableData.score = valueOf("videoScore");
            break;
        case Nodable.VIDEO_GAME:
            nodableData.title = valueOf("videoGameTitle");
            nodableData.creator = valueOf("videoGameCreator");
            nodableData.release_date = valueOf("videoGameReleaseDate");
            nodableData.status = valueOf("videoGameStatus");
            nodableData.grade = valueOf("videoGameGrade");
            nodableData.score = valueOf("videoGameScore");
            break;
    }
    nodeData.nodable_data = nodableData;
    nodeData.projectId = getLoadedProjectId();
    return JSON.stringify(nodeData);
}

function gatherNodeData() {
    // TODO: use urlEncodeInputValue()
    let nodableType = urlEncodeInputValue("nodeNodableType");
    let nodableData = "";
    switch(nodableType) {
        case Nodable.BOOK:
            let bookTitle = urlEncodeInputValue("bookTitle");
            let bookCreator = urlEncodeInputValue("bookCreator");
            let bookReleaseDate = urlEncodeInputValue("bookReleaseDate");
            nodableData = "&bookTitle="+bookTitle+"&bookCreator="+bookCreator+"&bookReleaseDate="+bookReleaseDate;
            break;
    }

    let media = urlEncodeInputValue("nodeMedia");
    let topic = urlEncodeInputValue("nodeTopic");
    let privacy = urlEncodeInputValue("nodePrivacy");
    let needMore = urlEncodeInputValue("nodeNeedMore");
    let title = urlEncodeInputValue("nodeTitle");
    let content = urlEncodeInputValue("nodeContent");
    let comment = urlEncodeInputValue("nodeComment");
    let tags = urlEncodeInputValue("nodeTags");

    let projectData = getAttachedProjectData();

    // Object.keys({myFirstName})[0] or Object.keys({myFirstName}).pop() will print the name of the variable as a string
    let data = "nodable_type="+nodableType+"&media="+media+"&topic="+topic+"&privacy="+privacy+"&needMore="+needMore+"&title="+title+"&content="+content+"&comment="+comment+"&tags="+tags+nodableData+projectData;
    return data;
}

/*********************************************************************************************************************/
/* Call-back functions                                                                                               */
/*********************************************************************************************************************/

function onNodeCreationResponse(xhr) {
    if (xhr.status === 201) {
        let json = JSON.parse(xhr.responseText);
        setLoadedNodeId(json.createdId);
        setNodeChangesAreSaved('true');
        showNodeSaveSaved();
        showNodeDelete();
        refreshProjectNodeList();
    } else {
        displayMessage(getXhrErrorMessage(xhr), "error");
    }
}

function onNodeUpdateResponse(xhr) {
    if (xhr.status === 204) {
        setNodeChangesAreSaved('true');
        showNodeSaveSaved();
        refreshProjectNodeList();
    } else {
        displayMessage(getXhrErrorMessage(xhr), "error");
    }
}

function onNodeDeletionResponse(xhr) {
    if (xhr.status === 204) {
        displayMessage("Node deleted.", "info");
        document.getElementById("nodeElement").style.visibility = "hidden" ;
        refreshProjectNodeList();
    } else {
        displayMessage(getXhrErrorMessage(xhr), "error");
    }
}

/*********************************************************************************************************************/
/* AJAX calls                                                                                                        */
/*********************************************************************************************************************/

function createNode(data) {
    let url = NodeRoutes.create();
    let xhr = getXMLHttpRequest('POST', url, true);
    xhr.onload = function() {
        onNodeCreationResponse(xhr);
    };
    xhr.send(data);
}

function updateNode(data) {
    let url = NodeRoutes.update(getLoadedNodeId());
    let xhr = getXMLHttpRequest('PUT', url, true);
    xhr.onload = function() {
        onNodeUpdateResponse(xhr);
    };
    xhr.send(data);
}

function deleteNode() {
    let loadedNodeId = getLoadedNodeId();
    if (loadedNodeId !== "") {
        let url = NodeRoutes.delete(loadedNodeId);
        let xhr = getXMLHttpRequest('DELETE', url);
        xhr.onload = function() {
            onNodeDeletionResponse(xhr);
        };
        xhr.send(null);
    } else {
        displayMessage("Failed to delete: cannot find loaded node", "error");
    }
}

function refreshProjectNodeList() {
    if (!isProjectView()) {
        return;
    }
    let url = NodeRoutes.readProject(getLoadedProjectId());
    let xhr = getXMLHttpRequest('GET', url);
    xhr.onload = function() {
        if (xhr.status === 200) {
            generateNodeListHtml(JSON.parse(xhr.responseText));
        } else {
            displayMessage("Failed to get node list: " + getXhrErrorMessage(xhr), "error");
            throw "Failed to getNodesForProject(), API response ("+xhr.status+"): " + xhr.response;
        }
    }
    xhr.send(null);
}

function loadNode(id) {
    if (id === '') {
        return false;
    }
    let unsavedChanges = !getNodeChangesAreSaved();
    let confirm = true;
    if (unsavedChanges) {
        confirm = window.confirm("Lose unsaved changes on current node?");
    }
    if (confirm) {
        let url = NodeRoutes.read(id);
        let xhr = getXMLHttpRequest('GET', url);
        xhr.onload = function() {
            if (xhr.status === 200) {
                showNode(JSON.parse(xhr.responseText));
            } else {
                displayMessage("Failed to get the node: " + getXhrErrorMessage(xhr), "error");
                throw "Failed to loadNode("+id+"), API response ("+xhr.status+"): " + xhr.response;
            }
        }
        xhr.send(null);
    }
    return true;
}

/*********************************************************************************************************************/
/* Getters and setters                                                                                               */
/*********************************************************************************************************************/

function getLoadedNodeId() {
    return document.getElementById("nodeId").value;
}
function setLoadedNodeId(nodeId) {
    document.getElementById("nodeId").value = nodeId;
}

/** returns false if unsaved changes are detected */
function getNodeChangesAreSaved() {
    return ('true' === document.getElementById("nodeChangesAreSaved").value);
}
/** set to true is just saved, false is unsaved change appeared */
function setNodeChangesAreSaved(saved) {
    document.getElementById("nodeChangesAreSaved").value = saved;
}

function getLoadedProjectId() {
    if (document.body.contains(document.getElementById('projectId'))) {
        return document.getElementById("projectId").value;
    } else {
        return null;
    }
}
function setLoadedProjectId(projectId) {
    if (document.body.contains(document.getElementById('projectId'))) {
        document.getElementById("projectId").value = projectId;
    }
}

function isProjectView() {
    try {
        return getLoadedProjectId() != null && getLoadedProjectId()  !== '';
    } catch (e) {
        return false
    }
}
// todo: delete if json is used
function getAttachedProjectData() {
    try {
        return "&projectId="+encodeURIComponent(getLoadedProjectId());
    } catch (error) {
        return "";
    }
}

/*********************************************************************************************************************/
/* DOM element display                                                                                               */
/*********************************************************************************************************************/


function showNodeSaveNeutral() {
    document.getElementById("nodeSaveIcon").classList.remove('green');
    document.getElementById("nodeSaveIcon").classList.remove('orange');
}

function showNodeSaveSaved() {
    showNodeSaveNeutral();
    document.getElementById("nodeSaveIcon").classList.add('green');
}

function showNodeSaveUnsaved() {
    showNodeSaveNeutral();
    document.getElementById("nodeSaveIcon").classList.add('orange');
}

function hideNodeDelete() {
    document.getElementById("nodeDeleteIcon").style.display = "none";
    document.getElementById("nodeDeleteConfirm").style.display = "none";
}

function showNodeDelete() {
    document.getElementById("nodeDeleteIcon").classList.remove("none");
    document.getElementById("nodeDeleteIcon").style.display = "inline-block";
    document.getElementById("nodeDeleteConfirm").style.display = "none";
}

function showNodeDeleteConfirm() {
    document.getElementById("nodeDeleteIcon").style.display = "none";
    document.getElementById("nodeDeleteConfirm").classList.remove("none");
    document.getElementById("nodeDeleteConfirm").style.display = "inline-block";
}