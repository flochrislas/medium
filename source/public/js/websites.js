/*********************************************************************************************************************
 *                                                                                                                   *
 *     JS for the websites speed dial page                                                                           *
 *                                                                                                                   *
 *********************************************************************************************************************/


/*********************************************************************************************************************/
/* Init actions                                                                                                      */
/*********************************************************************************************************************/

/*********************************************************************************************************************/
/* Events                                                                                                            */
/*********************************************************************************************************************/

function setWebsitesEditEventListeners(id) {
    document.getElementById("websiteEditTitle"+id)
        .addEventListener("keydown",
            (event) => updateWebsiteOnKeypress(event, id));
    document.getElementById("websiteEditUrl"+id)
        .addEventListener("keydown",
            (event) => updateWebsiteOnKeypress(event, id));
}

function updateWebsiteOnKeypress(event, id) {
    // If Enter key is pressed (event.keyCode 13)
    if (event.keyCode === 13) {
        // Ensure it is only this code that runs
        event.preventDefault();
        // Update website
        updateWebsite(id);
    }
    // If Escape key is pressed (event.keyCode 27)
    if (event.key === "Escape") {
        // Ensure it is only this code that runs
        event.preventDefault();
        // Cancel edit TODO
    }

}

function setWebsitesCreateEventListeners() {
    let id = 'New';
    document.getElementById("websiteEditTitle"+id)
        .addEventListener("keydown",
            (event) => createWebsiteOnKeypress(event));
    document.getElementById("websiteEditUrl"+id)
        .addEventListener("keydown",
            (event) => createWebsiteOnKeypress(event));
}

function createWebsiteOnKeypress(event) {
    // If Enter key is pressed (event.keyCode 13)
    if (event.key === "Enter") {
        // Ensure it is only this code that runs
        event.preventDefault();
        // Update website
        createWebsite();
    }
    // If Escape key is pressed (event.keyCode 27)
    if (event.key === "Escape") {
        // Ensure it is only this code that runs
        event.preventDefault();
        // Cancel creation
        resetWebsiteCreationForm();
    }
}

/*********************************************************************************************************************/
/* DOM actions                                                                                                       */
/*********************************************************************************************************************/

function generateWebsitesHtml(websites) {
    if (typeof websites === 'undefined') return "";
    let html = '';
    for(let i = 0; i < websites.length; ++i) {
        html +=
            '<div class="websiteElement" onclick="openWebsite(\''+websites[i].url+'\', '+websites[i].id+')">\n' +
            websites[i].title +
            '            <input class="websiteEditTitle" type="text" id="websiteEditTitle'+websites[i].id+'" value="'+websites[i].title+'" onclick="stopPropagation()">\n' +
            '            <input class="websiteEditUrl" type="text" id="websiteEditUrl'+websites[i].id+'" value="'+websites[i].url+'" onclick="stopPropagation()">\n' +
            '            <div class="websiteEdit" onclick="editWebsite('+websites[i].id+')"></div>\n' +
            '            <div class="websiteRemove" onclick="deleteWebsite('+websites[i].id+')"></div>\n' +
            '        </div>'
        ;
    }
    html += '<div id="websiteEditNew" class="websiteElement none">\n' +
        '                New website:\n' +
        '                <input class="websiteEditTitle" type="text" id="websiteEditTitleNew" value="" placeholder="Title">\n' +
        '                <input class="websiteEditUrl" type="text" id="websiteEditUrlNew" value="" placeholder="URL">\n' +
        '                <div class="websiteEdit" onclick="createWebsite()"></div>\n' +
        '                <div class="websiteRemove" onclick="resetWebsiteCreationForm()"></div>\n' +
        '    </div>\n';
    html += '<div id="websiteAdd" class="websiteElement add" onclick="showWebsiteCreationForm()">'+SvgIcons.plus()+'</div>\n';
    html += '<input type="hidden" id="activeWebsiteGroupId" value="'+getActiveWebsiteGroupId()+'">\n';
    return html;
}

function openWebsite(url, id) {
    if (document.getElementById("websiteEditTitle"+id).style.display !== 'block') {
        openNewPage(url);
    }
}

function editWebsite(id) {
    stopPropagation(); /* Prevents to open the URL from underneath div onclick */
    if (document.getElementById("websiteEditUrl"+id).style.display != 'block') {
        document.getElementById("websiteEditTitle"+id).style.display = 'block';
        document.getElementById("websiteEditUrl"+id).style.display = 'block';
        document.getElementById("websiteEditTitle"+id).focus();
        document.getElementById("websiteEditTitle"+id).select();
        setWebsitesEditEventListeners(id);
    } else {
        document.getElementById("websiteEditTitle"+id).style.display = 'none';
        document.getElementById("websiteEditUrl"+id).style.display = 'none';
        updateWebsite(id);
    }
}

/*********************************************************************************************************************/
/* Form data handling                                                                                                */
/*********************************************************************************************************************/


/*********************************************************************************************************************/
/* Call-back functions                                                                                               */
/*********************************************************************************************************************/

function refreshWebsitesFromResponse(xhr) {
    if (xhr.status === 200 || xhr.status === 201) {
        let response = JSON.parse(xhr.responseText);
        document.getElementById("websites").innerHTML = generateWebsitesHtml(response.websites);
        if (xhr.status === 201) {
            editWebsite(response.createdId);
        }
    } else {
        displayMessage(xhr.responseText);
    }
}

/*********************************************************************************************************************/
/* AJAX calls                                                                                                        */
/*********************************************************************************************************************/

function websiteShowFromGroup(groupId) {
    setActiveWebsiteGroupId(groupId);
    let url = WebsiteRoutes.readGroup(groupId);
    let xhr = getXMLHttpRequest('GET', url);
    xhr.onload = function() {
        refreshWebsitesFromResponse(xhr);
    };
    xhr.send();
}

function deleteWebsite(id) {
    stopPropagation(); /* Prevents to open the URL from underneath div onclick */
    let url = WebsiteRoutes.delete(id);
    let xhr = getXMLHttpRequest('DELETE', url);
    xhr.onload = function() {
        websiteShowFromGroup(getActiveWebsiteGroupId());
    };
    xhr.send(null);
}

function createWebsite() {
    let id ='New';
    let title = document.getElementById("websiteEditTitle"+id).value;
    let link = document.getElementById("websiteEditUrl"+id).value;
    resetWebsiteCreationForm();
    let url = WebsiteRoutes.create();
    let xhr = getXMLHttpRequest('POST', url);
    xhr.onload = function() {
        refreshWebsitesFromResponse(xhr);
    };
    xhr.send(encodeURI('url=' + link + '&title=' + title + '&groupId=' + getActiveWebsiteGroupId()));
}

function updateWebsite(id) {
    let title = document.getElementById("websiteEditTitle"+id).value;
    let link = document.getElementById("websiteEditUrl"+id).value;
    let url = WebsiteRoutes.update(id);
    let xhr = getXMLHttpRequest('PUT', url);
    xhr.onload = function() {
        refreshWebsitesFromResponse(xhr);
    };
    xhr.send(encodeURI('url=' + link + '&title=' + title + '&groupId=' + getActiveWebsiteGroupId()));
}

/*********************************************************************************************************************/
/* Getters and setters                                                                                               */
/*********************************************************************************************************************/

function getActiveWebsiteGroupId() {
    return document.getElementById("activeWebsiteGroupId").value;
}

function setActiveWebsiteGroupId(groupId) {
    document.getElementById("activeWebsiteGroupId").value = groupId;
}

/*********************************************************************************************************************/
/* DOM element display                                                                                               */
/*********************************************************************************************************************/

function showWebsiteCreationForm() {
    let id = 'New';
    document.getElementById("websiteEdit"+id).classList.remove('none');
    document.getElementById("websiteEditTitle"+id).style.display = 'block';
    document.getElementById("websiteEditUrl"+id).style.display = 'block';
    document.getElementById("websiteEditTitle"+id).focus();
    document.getElementById("websiteEditTitle"+id).select();

    document.getElementById("websiteAdd").classList.add('none');
}

function resetWebsiteCreationForm() {
    let id = 'New';
    document.getElementById("websiteEdit"+id).classList.add('none');
    document.getElementById("websiteEditTitle"+id).value = '';
    document.getElementById("websiteEditUrl"+id).value = '';
    document.getElementById("websiteAdd").classList.remove('none');
}