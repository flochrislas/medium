<?php

namespace App\Repositories;

use App\Todo;
use App\Project;
use App\Node;
use App\Nodable;
use App\Note;
use App\WebsiteGroup;

/**
 * Very basic class, made to access read data and return specifics collections to feed different pages across the app.
 * TODO: use a good pattern such as Repositories, Unit of Work, interfaces registered as a service and injected into controller as needed, etc... static is not good for scalability and testing, pattern is, plus its swappable
 * TODO: use cache in some cases
 *
 *
 * Class DataReader
 * @package App\Repositories
 */
class DataReader
{

    //---------------------------- MIXED ----------------------------//


    //---------------------------- TO-DO ----------------------------//

    public static function getAllTodos() {
        return Todo::all();
    }

    public static function getUndoneTodos() {
        return Todo::whereNull('done_date')->get();
    }

    public static function getDoneTodos() {
        return Todo::whereNotNull('done_date')->get();
    }


    //---------------------------- PROJECTS -------------------------//

    public static function getAllProjects() {
        return Project::all();
    }

    public static function getAllTasks(Project $project) {
        // TODO: implements properly with tasks or todos associated to the project
        return Todo::all();
    }


    //---------------------------- NODES ----------------------------//

    public static function nodesAll() {
        return Node::all();
    }

    public static function node(int $id) {
        return Node::find($id);
    }

    public static function nodeWithNodable(int $id) {
        return Node::with('nodable')->find($id);
    }

    public static function nodesForProjectId(int $projectId) {
        return Project::findOrFail($projectId)->nodes()->get();
    }

    public static function nodesForProject(Project $project) {
        return $project->nodes()->get();
    }

    //---------------------------- NODABLES ----------------------------//

    public static function getAllNodables() {
        return Nodable::all();
    }

    public static function getNodableByLabel($label) {
        return Nodable::whereLabel($label)->firstOrFail();
    }

    //---------------------------- NOTES (deprecated?) --------------//

    public static function getAllNotes() {
        return Note::all();
    }

    //---------------------------- WEBSITE GROUPS ----------------------------//

    public static function getAllWebsiteGroups() {
        return WebsiteGroup::orderBy('position', 'asc')->get();
    }

    public static function getWebsiteGroups($lowerPosition, $upperPosition) {
        return WebsiteGroup::whereBetween('position', [$lowerPosition, $upperPosition])->get();
    }

    public static function checkWebsiteGroupAtPosition($position) {
        return WebsiteGroup::where('position', $position)->exists();
    }

    public static function getWebsiteGroup($groupId) {
        return WebsiteGroup::find($groupId);
    }

    //---------------------------- WEBSITES ----------------------------//

    public static function getWebsitesFromGroup($groupId) {
        $group = WebsiteGroup::find($groupId);
        return $group->websites()->get();
    }

}
