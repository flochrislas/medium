<?php

namespace App\Traits;

/**
 * The idea here is to provide a simple way to give a model 'delete on cascade' on its polymorphic attribute(s)
 * A Laravel plugin exists: https://github.com/shiftonelabs/laravel-cascade-deletes/blob/master/src/CascadesDeletes.php
 * Class CascadeDelete
 * @package App\Traits
 */
trait CascadeDelete
{
    // TODO: implements multiple relations to cascade delete from the same model (cascadeDeleteRelationName array)
    public function delete() {
        if(parent::delete()) {
            $relationName = $this->cascadeDeleteRelationName;
            if ($relationName != null) {
                // $relation = method_exists($this, $relationName) ? $this->$relationName() : null;
                $relation = $this->$relationName();
                // The relation has the query with the where(id) already there
                $query = $relation->getQuery()->getQuery();
                // If there is no nodable associated, we should do nothing TODO: find a more elegant way
                if (($query->wheres[0])['type'] != 'Null') {
                    $query->delete();
                }
                // Using the model and delete will loop on this delete function (recursion)
                //$relationModel = $relation->getRelated();
                //$relatedEntity = $relationModel::find($this->nodable_id);
                //$relatedEntity->delete();
            }

        }
    }
}