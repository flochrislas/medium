<?php

namespace App\View\Components;

use Illuminate\View\Component;

/**
 * Class SvgIcon
 * @package App\View\Components
 *
 * Free OPen Source SVG icons:
 * https://feathericons.com/
 *
 */
class SvgIcon extends Component
{
    public $icon;
    public $width;
    public $height;
    public $viewBox;
    public $fill;
    public $stroke;
    public $strokeWidth;
    public $id;
    public $class;
    public $origin;

    /**
     * SvgIcon constructor. Create a new component instance.
     * @param string $icon
     * @param string $width
     * @param string $height
     * @param string $viewBox
     * @param string $fill
     * @param string $stroke
     * @param string $strokeWidth
     * @param string $id
     * @param string $class
     * @param string $origin
     * @return void
     */
    public function __construct(
        string $icon,
        string $width = null,
        string $height = null,
        string $viewBox = null,
        string $fill = null,
        string $stroke = null,
        string $strokeWidth = null,
        string $id = null,
        string $class = null,
        string $origin = null
    )
    {
        // Set default values for feathericons
        if ($origin == "feather") {
            $viewBox = $viewBox ?? "0 0 24 24";
            $fill = $fill ?? "none";
            $stroke = $stroke ?? "currentColor";
            $strokeWidth = $strokeWidth ?? "2";
        } else {
            $fill = $fill ?? "currentColor";
            $stroke = $stroke ?? "currentColor";
        }
        $this->icon = $icon;
        $this->width = $width;
        $this->height = $height;
        $this->viewBox = $viewBox;
        $this->fill = $fill;
        $this->stroke = $stroke;
        $this->strokeWidth = $strokeWidth;
        $this->id = $id;
        $this->class = $class;
        $this->origin = $origin;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.svg-icon');
    }
}
