<?php


namespace App\Services;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;


class DatabaseDump
{
    private const BACKUP_DISK = "public";
    private const BACKUP_DIR = "backups/";
    private string $dbName;
    private string $dbUser;
    private string $dbPwd;

    public function __construct()
    {
        $this->dbName = $_ENV['DB_DATABASE'];
        $this->dbUser = $_ENV['DB_USERNAME'];
        $this->dbPwd = $_ENV['DB_PASSWORD'];
    }

    public function backupWholeDb() {
        // prepare command
        $now = Carbon::now()->format('Y-m-d_H-i-s');
        $pathToFile = $this->getBackupsPath().$this->dbName."_$now.dump";
        $command = "export PGPASSWORD=$this->dbPwd; pg_dump -U \"$this->dbUser\" -Fc \"$this->dbName\" > \"$pathToFile\";";
        // to restore into new db: pg_restore -d newdb db.dump
        // execute command
        exec($command, $output, $status);
        if ($status === 0) {
            array_push($output, 'Command successful.', $pathToFile);
            return implode(PHP_EOL, $output);
        } else {
            $errorMessage = "backupWholeDb() failed (with status $status) command: ".$command.PHP_EOL.implode(PHP_EOL, $output);
            Log::error($errorMessage);
            return $errorMessage;
        }
    }

    public function backupDbTables(array $tables)
    {
        // prepare the command
        $now = Carbon::now()->format('Y-m-d_H-i-s');
        $pathToFile = $this->getBackupsPath().$this->dbName."_tables_$now.sql";
        $tablesString = " -t ".implode(" -t ", $tables);
        $command = "export PGPASSWORD=$this->dbPwd; pg_dump -U \"$this->dbUser\" $tablesString \"$this->dbName\" > \"$pathToFile\";";
        // to restore into new db: psql -d newdb -f db.sql
        // https://www.postgresql.org/docs/10/app-pgdump.html
        // execute command
        exec($command, $output, $status);
        if ($status === 0) {
            array_push($output, 'Command successful.', $pathToFile);
            return implode(PHP_EOL, $output);
        } else {
            $errorMessage = "backupDbTables() failed (with status $status) command: ".$command.PHP_EOL.implode(PHP_EOL, $output);
            Log::error($errorMessage);
            return $errorMessage;
        }
    }

    public function deleteDumpFiles() {
        // Get all files in a directory
        $files = $this->getBackupsDisk()->allFiles(self::BACKUP_DIR);
        // Delete Files
        $success = $this->getBackupsDisk()->delete($files);
        if ($success) {
            return "Command successful.";
        } else {
            $errorMessage = "deleteDumpFiles() failed command: Storage::delete";
            Log::error($errorMessage);
            return $errorMessage;
        }
    }

    /**
     * Gets the full path to the backups directory
     * @return string path
     */
    private function getBackupsPath() {
        return $this->getBackupsDisk()->path(self::BACKUP_DIR);
    }

    /**
     * Gets the disk, aka Filesystem, for the backups directory
     * @return \Illuminate\Contracts\Filesystem\Filesystem|\Illuminate\Filesystem\FilesystemAdapter
     */
    private function getBackupsDisk() {
        return Storage::disk(self::BACKUP_DISK);
    }
}