<?php

namespace App\Http\Controllers;

use App\Todo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Repositories\DataReader;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Log::debug('TodoController::index()');
        $todos = DataReader::getUndoneTodos();
        $dones = DataReader::getDoneTodos();
        return view('todos.index', compact('todos', 'dones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $todo = new Todo;
        $todo->content = $request["content"];
        $todo->priority = $request["priority"];
        $todo->difficulty = $request["difficulty"];
        $todo->weight = $request["weight"];
        $todo->category = $request["category"];
        $todo->project = $request["project"];
        $todo->save();

        return $this->getListsJson();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function show(Todo $todo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function edit(Todo $todo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Todo $todo)
    {
        //$todo = Todo::find($todo);
        /*
        $todo->content = $request["content"];
        $todo->priority = $request["priority"];
        $todo->difficulty = $request["difficulty"];
        $todo->weight = $request["weight"];
        $todo->category = $request["category"];
        $todo->project = $request["project"];
        $todo->save();*/
        $todo->update($request->all());

        return $this->getListsJson();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Todo $todo
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Todo $todo)
    {
        // remove
        $todo->delete();

        return $this->getListsJson();
    }

    public function done(Todo $todo)
    {
        // mark as done
        $todo = Todo::find($todo->id);
        $todo->done_date =  now("UTC");
        $todo->save();

        return $this->getListsJson();
    }

    public function undone(Todo $todo)
    {
        // revert mark as done
        $todo = Todo::find($todo->id);
        $todo->done_date =  null;
        $todo->save();

        return $this->getListsJson();
    }

    private function getListsJson() {
        return response()->json(array(
            'todos' => DataReader::getUndoneTodos(),
            'dones' => DataReader::getDoneTodos(),
        ));
    }
}
