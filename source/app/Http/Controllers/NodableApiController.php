<?php

namespace App\Http\Controllers;

use App\Nodable;
use App\Repositories\DataReader;
use Illuminate\Http\Request;

/**
 * Class NodableApiController
 * @package App\Http\Controllers
 */
class NodableApiController extends Controller
{

    public function readAll() {
        $everything =  DataReader::getAllNodables();
        return response()->json($everything);
    }
}
