<?php

namespace App\Http\Controllers;

use App\Nodables\Website;
use App\Node;
use App\Factories\NodableFactory;
use App\Repositories\DataReader;
use App\WebsiteGroup;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class WebsiteApiController extends JsonApiController {

    private function validateWebsiteData(array $websiteData) {
        return Validator::make($websiteData, [
            'content' => 'url',
            'title' => 'string',
            'groupId' => 'integer'
        ]);
    }

    /**
     * API returning JSON for all websites belonging to a group
     * @param $groupId
     * @return JsonResponse
     */
    public function group($groupId) {
        $websites = DataReader::getWebsitesFromGroup($groupId);
        return response()->json(compact('websites'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create(Request $request) {
        // Validates
        $validator = $this->validateWebsiteData($request->all());
        if ($validator->fails()) {
            // throw exception , or use error bag , check laravel validation
            $errorMessage = 'Failed to store website: wrong parameter ' + $validator->errors()->getMessages()[0];
            return $this->errorResponse($errorMessage, Response::HTTP_BAD_REQUEST);
        }

        $websiteNode = new Node;
        $websiteNode->content = $request->url;
        $websiteNode->title = $request->title;
        $websiteNode->media = 'website';
        $websiteNode->comment = 'This node has been created automatically, as a website.';
        $websiteNode->need_more = false;
        $websiteNode->privacy = 1;

        $groupId = $request->groupId;
        $group = DataReader::getWebsiteGroup($groupId);
        $websiteNode->topic = $group->title;
        $websiteNode->tags = $group->title;

        if ($websiteNode->save()) {
            $createdId = $websiteNode->id;
            // Deals with nodable
            if ($this->saveWebsiteNodable($request, $websiteNode, $group)) {
                $websites = DataReader::getWebsitesFromGroup($groupId);
                return response()->json(compact('websites', 'createdId'), Response::HTTP_CREATED);
            } else {
                $errorMessage = 'Stored node ID '.$createdId.', but failed to store nodable of type Website.';
                return $this->errorResponse($errorMessage);
            }

        } else {
            return $this->errorResponse('Failed to store website.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return void
     */
    public function update(Request $request, int $id) {
        $validator = $this->validateWebsiteData($request->all());
        if ($validator->fails()) {
            // throw exception , or use error bag , check laravel validation
            return response('Failed to update website: wrong parameter '+$validator->errors()->getMessages()[0], Response::HTTP_BAD_REQUEST);
        }
        // TODO: better handle errors
        // push = save + save model relations
        $website = Website::findOrFail($id);
        $node = $website->node()->get()[0];
        $website->title = $request->input('title');
        $website->url = $request->input('url');
        $website->push();
        $node->title = $request->input('title');
        $node->content = $request->input('url');
        $node->push();
        //$test = $website->update(['content' => $request->url, 'title'=>$request->title]);
        $websites = DataReader::getWebsitesFromGroup($request->groupId);
        return response()->json(compact('websites'));
    }

    /**
     * Remove the website and its associated node (that gets detached from its potential associated project)
     *
     * @param int $id
     * @return Response
     */
    public function delete(int $id) {
        try {
            $website = Website::findOrFail($id);
            $node = $website->node()->get()[0];
            $node->projects()->detach();
            $node->delete();
            return response()->noContent();
        } catch (\Exception $e) {
            $errorMessage = $e->getMessage();
            return $this->errorResponse($errorMessage);
        }

    }

    /**
     * @param Request $request
     * @param Node $websiteNode
     * @param WebsiteGroup $group
     * @return bool
     */
    private function saveWebsiteNodable(Request $request, Node $websiteNode, WebsiteGroup $group) : bool {
        // Gets an instance: gather all the fields we needs and validate them and put them into a model
        $nodableType = 'Website';
        $nodableData = array(
            'title' => $request->title,
            'url' => $request->url,
            'thumbnail' => $request->thumbnail,);
        $nodable = NodableFactory::getInstance($nodableType, $nodableData);
        // Saves the nodable and update the node
        try {
            if ($group != null) {
                // model needs to be saved before can attach smth
                $nodable->saveOrFail();
                $nodable->websiteGroups()->attach($group);
            }
            if ($nodable->node()->save($websiteNode)) {
                return true;
            }
        } catch (\Throwable $exception) {
            // todo: log error?
            $error = $exception->getMessage();
            return false;
        }
        return false;
    }
}