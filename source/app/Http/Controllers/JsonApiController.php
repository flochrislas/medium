<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class JsonApiController extends Controller {
    protected function errorResponse(string $message, int $status = Response::HTTP_INTERNAL_SERVER_ERROR) : JsonResponse {
        $errorMessage = $message;
        return response()->json(compact('errorMessage'), $status);
    }
}