<?php

namespace App\Http\Controllers;

use App\Node;
use App\Repositories\DataReader;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use View;

/**
 * Class NodeController
 * @package App\Http\Controllers
 */
class NodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        return view('nodes.create', [
            'nodables' => DataReader::getAllNodables()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return View
     */
    public function show(int $id)
    {
        return view('nodes.show', [
            'id' => $id,
            'nodables' => DataReader::getAllNodables()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Node  $node
     * @return Response
     */
    public function edit(Node $node)
    {
        //
    }

}
