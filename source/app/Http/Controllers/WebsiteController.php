<?php

namespace App\Http\Controllers;

use App\Repositories\DataReader;
use App\WebsiteGroup;
use Illuminate\Http\Response;

/**
 * Class WebsiteController
 * @package App\Http\Controllers
 *
 * A website is now a node, of the 'website' media type, and a node belonging to a WebsiteGroup.
 * This controller gathers methods to handle data requests for nodes as websites.
 *
 */
class WebsiteController extends Controller
{
    /**
     * To display the WEBSITES page     *
     * @return Response
     */
    public function index() {
        $websiteGroups = DataReader::getAllWebsiteGroups();
        if (empty($websiteGroups)) {
            $defaultGroup = new WebsiteGroup;
            $defaultGroup->title = 'Unsorted';
            $defaultGroup->save();
            $websites = [];
        } else {
            // TODO: improve that with user cache DB of some sort that will save last active group etc...
            $websites = $websiteGroups[0]->websites()->get();
        }
        return view('websites.index', compact('websites', 'websiteGroups'));
    }

}
