<?php

namespace App\Http\Controllers;

use App\Factories\NodableFactory;
use App\Nodables\Nodable;
use App\Node;
use App\Repositories\DataReader;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;

/**
 * Class NodeApiController
 * @package App\Http\Controllers
 */
class NodeApiController extends JsonApiController {
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return JsonResponse
     */
    public function create(Request $request) {
        $request->validate([
            'media'=>'required',
            'topic'=>'required',
            'title'=>'required',
            'content'=>'required'
        ]);

        $node = new Node([
            'media' => $request->input('media'),
            'topic' => $request->input('topic'),
            'privacy' => $request->input('privacy'),
            'need_more' => $request->input('need_more'),
            'title' => $request->input('title'),
            'content' => $request->input('content'),
            'comment' => $request->input('comment'),
            'tags' => $request->input('tags')
        ]);
        $node->need_more = false;
        $projectId = $request->input('projectId');
        $nodableType = $request->input('nodable_type');

        if ($node->save()) {
            $createdId = $node->id;
            // Attaches project if needed
            if ($projectId != null) {
                $node->projects()->attach($projectId);
            }
            // Deals with nodable
            if ($this->saveNodable($nodableType, $request, $node)) {
                return response()->json(compact('createdId'), Response::HTTP_CREATED);
            } else {
                $errorMessage = 'Stored node ID '.$createdId.', but failed to store nodable of type "'.$nodableType.'".';
                return $this->errorResponse($errorMessage);
            }

        } else {
            $errorMessage = 'Failed to store node.';
            return $this->errorResponse($errorMessage);
        }
        // TODO: handle errors with exceptions and / or errors bags
    }


    /**
     * Create and save the proper nodable if needed
     * @param ?string $nodableType
     * @param Request $request the node creation request
     * @param Node $node
     * @return bool
     * @throws \Throwable
     */
    private function saveNodable(?string $nodableType, Request $request, Node $node) : bool {

        if ($nodableType == null || $nodableType == '') {
            return true;
        }

        // Gets an instance: gather all the fields we needs and validate them and put them into a model
        $nodable = NodableFactory::getInstance($nodableType, $request->input('nodable_data'));

        // Saves the nodable and update the node
        try {
            $nodable->saveOrFail();
            if ($nodable->node()->save($node)) {
                return true;
            }
        } catch (\Exception $exception) {
            // todo: log error?
            $error = $exception->getMessage();
            return false;
        }
    }


    /**
     * Read the specified resource.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function read(int $id) {
        $node = DataReader::nodeWithNodable($id);
        $node->nodable_type = Nodable::hideNamespace($node->nodable_type);
        return response()->json($node);
    }

    public function readForProject($projectId) {
        $nodeList = DataReader::nodesForProjectId($projectId);
        return response()->json($nodeList);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, int $id) {
        // https://laravel.com/docs/7.x/validation#quick-ajax-requests-and-validation
        $updateData = $request->validate([
            'media'=>'nullable',
            'topic'=>'nullable',
            'title'=>'nullable',
            'content'=>'nullable',
            'nodable_type'=>'nullable',
            'need_more'=>'nullable',
            'comment'=>'nullable',
            'tags'=>'nullable'
        ]);
        // old easy way when it was simple
        //Node::whereId($id)->update($updateData);

        $node = DataReader::node($id);
        if ($node == null) {
            $errorMessage = 'Node '.$id.' not found.';
            return $this->errorResponse($errorMessage, Response::HTTP_NOT_FOUND);
        }

        $originNodableType = $node->nodableSimpleName();
        $requestedNodableType = $request->input('nodable_type');

        // In case of changing nodable type
        if ($requestedNodableType == null && $originNodableType != null) {
            $node->nodable()->getQuery()->getQuery()->delete();
        }

        $updateResult = $node->update($updateData);
        if (!$updateResult) {
            $errorMessage = 'Failed updating node '.$id;
            return $this->errorResponse($errorMessage);
        }

        if ($requestedNodableType != null) {
            if ($originNodableType == $requestedNodableType) {
                // we update existing nodable
                // first we get a valid instance from the data
                $nodable = $this->getNodableInstance($requestedNodableType, $request->input('nodable_data'));
                // then we update from the valid instance data
                $nodableResult = $node->nodable()->getQuery()->getQuery()->update($nodable->toArray());
                if ($nodableResult < 1) {
                    $errorMessage = 'Failed updating nodable for node '.$id;
                    return $this->errorResponse($errorMessage);
                }
            } else {
                // we create new nodable
                $nodableResult = $this->saveNodable($requestedNodableType, $request, $node);
                if ($nodableResult == null) {
                    $errorMessage = 'Failed saving nodable '.$requestedNodableType.' for node id '.$id;
                    return $this->errorResponse($errorMessage);
                }
            }
        }

        return response()->noContent();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete(int $id) {
        try {
            $node = Node::findOrFail($id);
            $node->projects()->detach();
            $node->delete();
            return response()->noContent();
        } catch (\Exception $e) {
            $errorMessage = $e->getMessage();
            return $this->errorResponse($errorMessage);
        }

    }
}
