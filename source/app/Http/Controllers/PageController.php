<?php

namespace App\Http\Controllers;

use App\Repositories\DataReader;

class PageController extends Controller
{
    private string $agent = 'desktop';

    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth'); // we handle auth in route group in web.php
        // $agent = $this->middleware('deviceDetector'); // just an idea to redirect to different view for mobile or desktop
    }

    public function home() {
        if ($this->agent == 'desktop') {
            return $this->desktopHome();
        } else {
            return $this->mobileHome();
        }

    }

    private function mobileHome() {
        return view('mobile.home');
    }

    private function desktopHome() {
        return view('desktop.home', [
            'projects' => DataReader::getAllProjects(),
            'nodes' => DataReader::nodesAll(),
            'todos' => DataReader::getUndoneTodos(),
            'dones' => DataReader::getDoneTodos()
        ]);
    }
}