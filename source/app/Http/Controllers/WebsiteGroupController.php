<?php

namespace App\Http\Controllers;

use App\Repositories\DataReader;
use App\WebsiteGroup;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

/**
 * Class WebsiteGroupController
 * @package App\Http\Controllers
 * TODO: separate JSON restful API requests from UI stuff.
 * TODO: use json for restful API error responses.
 */
class WebsiteGroupController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // TODO: parameter validation
        $websiteGroup = new WebsiteGroup;
        $websiteGroup->title = $request->title;
        if ($websiteGroup->save()) {
            $createdId = $websiteGroup->id;
            // Sets the position to the created id
            $websiteGroup->update(['position'=>$createdId]);
            // Returns new list
            $websiteGroups = DataReader::getAllWebsiteGroups();
            return response()->json(compact('websiteGroups','createdId'), Response::HTTP_CREATED);
        } else {
            return response('Failed to store websiteGroup.', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WebsiteGroup  $websiteGroup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WebsiteGroup $websiteGroup)
    {
        // TODO: parameter validation
        if ($websiteGroup->update(['title'=>$request->title])) {
            return response()->noContent();
        } else {
            return response('Failed to update websiteGroup.', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WebsiteGroup  $websiteGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebsiteGroup $websiteGroup)
    {
        // The default group should not be allowed to be removed
        if ($websiteGroup->id == 1) {
            return response('The Default group cannot be removed', Response::HTTP_FORBIDDEN);
        }
        // First all websites belonging to that group should be moved to the default group
        DataReader::getWebsiteGroup(1)->websites()->attach($websiteGroup->websites);
        // Deletes group
        try {
            $websiteGroup->delete();
            $websiteGroups = DataReader::getAllWebsiteGroups();
            return response()->json(compact('websiteGroups'));
        } catch (\Exception $e) {
            return response('Failed to destroy websiteGroup: '.$e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }

    public function updatePosition(Request $request) {
        $position = $request->position;
        if (DataReader::checkWebsiteGroupAtPosition($position)) {
            $groupId = $request->groupId;
            $this->updatePositionsCascade($groupId, $position);
            $websiteGroups = DataReader::getAllWebsiteGroups();
            return response()->json(compact('websiteGroups'));
        } else {
            // If the destination does not exist, we do nothing
            return response()->noContent();
        }


    }

    private function updatePositionsCascade($groupId, $position) {
        $movingGroup = DataReader::getWebsiteGroup($groupId);
        $positionOrigin = $movingGroup->position;

        if ($positionOrigin < $position) {
            foreach(DataReader::getWebsiteGroups($positionOrigin+1, $position) as $websiteGroup) {
                $websiteGroup->update(['position'=>DB::raw('position-1')]);
            }
        } else {
            foreach(DataReader::getWebsiteGroups($position, $positionOrigin-1) as $websiteGroup) {
                $websiteGroup->update(['position'=>DB::raw('position+1')]);
            }
        }

        $movingGroup->update(['position'=>$position]);
    }
}
