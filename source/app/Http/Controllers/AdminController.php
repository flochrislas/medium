<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Artisan;

class AdminController extends Controller
{
    private const DEFAULT_TABLES = ["nodes", "website_groups", "website_website_group"];

    public function downloadFile(Request $request) {
        $path = urldecode($request->path);
        return response()->download($path);
    }

    public function runDbDump() {
        $command = 'dump:db';
        Artisan::call($command);
        $commandsOutput = $this->getDumpCommandsOutput();
        $filePath = $commandsOutput['filePath'];
        $message = $commandsOutput['message'];
        return response()->json(compact('message', 'filePath'));
    }

    public function runDbTablesDump(Request $request)
    {
        $tables = $request->tables;
        if (!isset($tables)) {
            $tables = self::DEFAULT_TABLES;
        }
        $command = 'dump:db';
        $params = [
            '--t' => $tables
        ];
        Artisan::call($command, $params);
        $commandsOutput = $this->getDumpCommandsOutput();
        $filePath = $commandsOutput['filePath'];
        $message = $commandsOutput['message'];
        return response()->json(compact('message', 'filePath'));
    }

    public function clearBackups() {
        $command = 'clear:backups';
        Artisan::call($command);
        $message = $this->getCommandsOutput();
        return response()->json(compact('message'));
    }

    public function clearCache() {
        /*
        Artisan::call('config:clear'); // config:cache would then cache again after clearing
        $commandsOutput = $this->getCommandsOutput();
        Artisan::call('view:clear'); // view:cache would then cache again after clearing
        $commandsOutput = $commandsOutput . "<br>" . $this->getCommandsOutput();
        Artisan::call('route:clear'); // route:cache would then cache again after clearing
        $commandsOutput = $commandsOutput . "<br>" . $this->getCommandsOutput();
        Artisan::call('cache:clear'); // clear memcached
        $commandsOutput = $commandsOutput . "<br>" . $this->getCommandsOutput();
        */
        // The following command seems to include all the above + optimize command:
        Artisan::call('optimize:clear'); //
        $message = $this->getCommandsOutput();
        return response()->json(compact('message'));
    }

    public function restartMemcached() {
        // check config cache is memcached
        // check memcached is not already running (status from sudo systemctl status memcached)
        // check /var/run/memcached exists
        // if not then sudo mkdir /var/run/memcached; sudo chown -R memcached:memcached /var/run/memcached;
        // then sudo systemctl start memcached
        // then check status again
    }

    public function index()
    {
        return view('admin.index');
    }

    private function getCommandsOutput() {
        return str_replace(PHP_EOL, '<br>', Artisan::output());
    }

    private function getDumpCommandsOutput() {
        $array = explode(PHP_EOL, trim(Artisan::output()));
        $filePath = end($array);
        $message = implode('<br>', $array);
        $url = route('admin.downloadFile', ['path' => urlencode($filePath)]);
        $linked = "<a href=\"$url\">$filePath</a>";
        $message = str_replace($filePath, $linked, $message);
        return compact('message', 'filePath');
    }
}