<?php

namespace App\Nodables;

class Movie extends NodableCreation {
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'status','title','director','release_date','grade','score'
    ];

}
