<?php

namespace App\Nodables;

class Video extends NodableCreation {
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'status','title','link','creator','release_date','grade','score'
    ];

}
