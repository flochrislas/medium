<?php

namespace App\Nodables;

class Person extends Nodable {
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name','contact','birth_date'
    ];

}
