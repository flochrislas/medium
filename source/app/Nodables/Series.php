<?php

namespace App\Nodables;

class Series extends NodableCreation {
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'status','title','release_date','grade','score'
    ];

}
