<?php

namespace App\Nodables;

class Music extends NodableCreation {
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'status','artist','release_date','grade','score'
    ];

}
