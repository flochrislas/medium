<?php

namespace App\Nodables;

use App\WebsiteGroup;

class Website extends Nodable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'url','title','thumbnail'
    ];

    /***********************************************************/
    /*  Node, as a website, can be linked to a website group   */
    /***********************************************************/

    /**
     * The groups to which belong the website
     * Read from the DB
     */
    public function websiteGroups() {
        return $this->belongsToMany(\App\WebsiteGroup::class);
    }

    /**
     * Attach the given group (that must already exist) to the node
     * @param $groupIds
     */
    /**
     * @param WebsiteGroup $group
     */
    public function attachWebsiteGroup(WebsiteGroup $group) {
        $this->websiteGroups()->attach($group);
    }

    /**
     * Attach the given groups (that must already exist) to the node
     * @param $groupIds
     */
    public function attachWebsiteGroups($groupIds) {
        if (!empty($groupIds)) {
            // Get tags from the DB
            $query = (new WebsiteGroup)->newQuery();
            foreach ($groupIds as $groupId) {
                $query->orWhere('id', '=', $groupId);
            }
            $websiteGroups = $query->get();
            // Attach them
            $this->websiteGroups()->attach($websiteGroups);
        }
        // Note:
        // attach just associate existing records: http://laraveldaily.com/pivot-tables-and-many-to-many-relationships/
        // otherwise use save or saveMany
        // awesome https://m.dotdev.co/writing-advanced-eloquent-search-query-filters-de8b6c2598db
    }
}
