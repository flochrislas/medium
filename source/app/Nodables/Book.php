<?php

namespace App\Nodables;

class Book extends NodableCreation {
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'status','title','author','release_date','grade','score'
    ];

}
