<?php

namespace App\Nodables;

use Illuminate\Database\Eloquent\Model;

class Nodable extends Model
{
    /**
     * Returns the Nodables namespace with a trailing backslash
     * @return string ex: 'App\Nodables\'
     */
    static public function getNamespace() : string {
        return __NAMESPACE__ . '\\';
    }

    /**
     * @param ?string $nodableType ex: 'book' or 'Book'
     * @return string ex: 'App\Nodables\Book'
     */
    static public function getNamespacedClassName(?string $nodableType) : string {
        // todo: check if backslash are present, if so, check if namespace is correct, if so, directly return string as is
        return self::getNamespace().ucfirst($nodableType);
    }

    /**
     * @param ?string $nodableType ex: 'App\Nodables\Book'
     * @return string ex: 'Book'
     */
    static public function hideNamespace(?string $nodableType) : string {
        return str_replace(self::getNamespace(), '', $nodableType);
    }


    /**
     * For polymorphism.
     * See: https://stackoverflow.com/questions/26691577/how-can-i-implement-single-table-inheritance-using-laravels-eloquent
     * Note: this function used to be needed for each nodable classes such as Book, Series etc...
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function node() {
        return $this->morphOne('App\Node', 'nodable');
    }
}