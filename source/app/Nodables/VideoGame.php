<?php

namespace App\Nodables;

class VideoGame extends NodableCreation {
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'status','title','creator','platform','release_date','grade','score'
    ];

}
