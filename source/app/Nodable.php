<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Nodable
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $label Label that will be used in the UI.
 * @property string $model Class name of the model in the implementation.
 * @property string $table Table name containing the data in the database.
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Nodable newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Nodable newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Nodable query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Nodable whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Nodable whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Nodable whereLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Nodable whereModel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Nodable whereTable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Nodable whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Nodable extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'label','model','table'
    ];

    /**
     * The attributes that will be (kinda) hidden from views
     *
     * @var array
     */
    protected $hidden = [
        'table'
    ];
}
