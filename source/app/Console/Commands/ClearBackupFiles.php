<?php

namespace App\Console\Commands;

use App\Services\DatabaseDump;
use Illuminate\Console\Command;

class ClearBackupFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear:backups';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deletes all files from the backups directory (clearing db dumps).';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dbDumpService = new DatabaseDump();
        $this->info("Deleting all files from the backups directory (clearing db dumps)...");
        $output = $dbDumpService->deleteDumpFiles();
        $this->info($output);
    }
}
