<?php

namespace App\Console\Commands;

use App\Services\DatabaseDump;
use Illuminate\Console\Command;

/**
 * Class DumpDb
 * @package App\Console\Commands
 * https://laravel.com/docs/7.x/artisan#defining-input-expectations
 */
class DumpDb extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dump:db {--t=*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dump the application\'s database, the entire thing, or just some of its tables.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Read arguments
        $tables = $this->option('t');
        // Call service
        $dbDumpService = new DatabaseDump();
        if (!isset($tables) || empty($tables)) {
            $this->info("Backing up the entire database...");
            $output = $dbDumpService->backupWholeDb();
        } else {
            $this->info("Backing up selected database tables...");
            $output = $dbDumpService->backupDbTables($tables);
        }
        $this->info($output);
    }
}
