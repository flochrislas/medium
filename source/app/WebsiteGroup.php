<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Nodables\Website;

class WebsiteGroup extends Model {
    protected $fillable = [
        'title', 'position'
    ];

    /**
     * The websites that belong to the group.
     * Note that in PHP you must use ->get() on that to get the actual collection,
     * or ->attach(...) to add some website to the group.
     */
    public function websites() {
        return $this->belongsToMany(Website::class);
    }
}
