<?php


namespace App\Factories;

use App\Nodables\Nodable;
use App\Nodables\NodableCreation;
use App\Nodables\Person;
use App\Nodables\Website;
use Illuminate\Support\Facades\Validator;

class NodableFactory {

    static public function getEmptyInstance(?string $nodableType) {
        if ($nodableType == null || $nodableType == '') {
            return null;
        }
        $class = Nodable::getNamespacedClassName($nodableType);
        return new $class();
    }

    static public function getInstance(?string $nodableType, array $nodableData) {

        $nodable = self::getEmptyInstance($nodableType);

        // First we set fields based on potential nodable type CATEGORY (trait)
        switch (true) {
            case $nodable instanceof NodableCreation:
                self::setNodableCreationFields($nodable, $nodableData);
                break;
        }

        // Then we set fields specific to nodable type itself
        switch ($nodableType) {
            case 'Person':
                self::setPersonFields($nodable, $nodableData);
                break;
            case 'Website':
                self::setWebsiteFields($nodable, $nodableData);
                break;
        }

        return $nodable;
    }

    static private function setPersonFields(Person $nodable, array $nodableData) : void {
        // Validates
        $validator = Validator::make($nodableData, [
            'name' => 'nullable',
            'contact' => 'nullable',
            'birth_date' => 'nullable|date'
        ]);
        if ($validator->fails()) {
            // throw exception , or use error bag , check laravel validation
        }
        // Sets
        $nodable->name = $nodableData['name'];
        $nodable->contact = $nodableData['contact'];
        $nodable->birth_date = $nodableData['birth_date'];
    }

    static private function setWebsiteFields(Website $nodable, array $nodableData) : void {
        // Validates
        $validator = Validator::make($nodableData, [
            'title' => 'required',
            'url' => 'url',
            'thumbnail' => 'nullable'
        ]);
        if ($validator->fails()) {
            // throw exception , or use error bag , check laravel validation
        }
        // Sets
        $nodable->title = $nodableData['title'];
        $nodable->url = $nodableData['url'];
        $nodable->thumbnail = $nodableData['thumbnail'];
    }

    static private function setNodableCreationFields(NodableCreation $nodable, array $nodableData) : void {
        // Validates
        $validator = Validator::make($nodableData, [
            'title' => 'nullable',
            'creator' => 'nullable',
            'release_date' => 'nullable|date',
            'status' => 'nullable|integer|min:0|digits_between: 0,2',
            'grade' => 'nullable|integer|min:0|digits_between: 0,4',
            'score' => 'nullable|integer',
        ]);
        if ($validator->fails()) {
            // throw exception , or use error bag , check laravel validation
        }
        // Sets
        try {$nodable->title = $nodableData['title'];} catch (\Exception $e) {/* we just skip */}
        try {$nodable->creator = $nodableData['creator'];} catch (\Exception $e) {/* we just skip */}
        try {$nodable->release_date = $nodableData['release_date'];} catch (\Exception $e) {/* we just skip */}
    }
}