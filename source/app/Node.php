<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Node extends Model
{
    use Traits\CascadeDelete;
    protected string $cascadeDeleteRelationName = 'nodable';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'media','topic','privacy','need_more','title','content','comment','tags'
    ];

    /*************************************************/
    /*  Projects n-n                                 */
    /*************************************************/

    /**
     * n-n relation with Project
     * This returns all the projects using this node
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function projects() {
        return $this->belongsToMany(Project::class);
    }

    /*************************************************/
    /*  Nodables  polymorphic                        */
    /*************************************************/

    /**
     * This is for polymorphism.
     * See: https://stackoverflow.com/questions/26691577/how-can-i-implement-single-table-inheritance-using-laravels-eloquent
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function nodable() {
        return $this->morphTo();
    }

    /**
     * Gets the name of the nodable type without the stuff before the last \
     * @return string|null
     */
    public function nodableSimpleName() {
        $fullName = $this->nodable_type;
        if ($fullName == null) {
            return null;
        }
        $pos = strrpos($fullName, '\\');
        return $pos === false ? $fullName : substr($fullName, $pos + 1);
    }
}
