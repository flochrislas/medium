<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Todo
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $content
 * @property string|null $done_date
 * @property int|null $priority
 * @property int|null $difficulty
 * @property int|null $weight
 * @property int|null $category
 * @property int|null $project
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Todo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Todo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Todo query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Todo whereCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Todo whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Todo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Todo whereDifficulty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Todo whereDoneDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Todo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Todo wherePriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Todo whereProject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Todo whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Todo whereWeight($value)
 * @mixin \Eloquent
 */
class Todo extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'content','priority','difficulty','weight','category','project',
    ];
}
