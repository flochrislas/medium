@extends('desktop.base')

@push('meta')
    <title>{{config('app.name')}} - Todo</title>
    <meta name="description" content="To-do list">
@endpush

@push('style')
    <link href="/css/todos.css" rel="stylesheet">
@endpush

@section('content')
    <div class="todosBlock">

        <div id="message"></div>
        <br>

        <button onclick="todoAdd('new task')" type="submit">Add</button>
        <br>

        <div id="todos">
            <table class="todosTable">
                @foreach($todos as $todo)
                    <tr>
                        <td>
                            <input class="todoContent" type="text" id="todoContent{{$todo->id}}" value="{{$todo->content}}">
                        </td>
                        <td class="smallestCell">
                            <button onclick="todoSave({{$todo->id}})" type="submit">Save</button>
                        </td>
                        <td class="smallestCell">
                            <button onclick="todoDone({{$todo->id}})" type="submit">Done</button>
                        </td>
                        <td class="smallestCell">
                            <button onclick="todoDestroy({{$todo->id}})" type="submit">Remove</button>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>

        <br><br>

        <div id="dones">
            <table class="todosTable">
                @foreach($dones as $todo)
                    <tr>
                        <td>
                            {{$todo->content}}
                        </td>
                        <td class="smallestCell">
                            {{$todo->done_date}}
                        </td>
                        <td class="smallestCell">
                            <button onclick="todoDone({{$todo->id}})" type="submit">Undo</button>
                        </td>
                        <td class="smallestCell">
                            <button onclick="todoDestroy({{$todo->id}})" type="submit">Remove</button>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>

    </div>


    <script>

        function generateTodosHtml(todos) {
            if (typeof todos === 'undefined') return "";
            let html = '<table class="todosTable">';
            for(let i = 0; i < todos.length; ++i) {
                html += '<tr>\n' +
                    '                    <td>\n' +
                    '                        <input class="todoContent" type="text" id="todoContent'+todos[i].id+'" value="'+todos[i].content+'">\n' +
                    '                    </td>\n' +
                    '                    <td class="smallestCell">\n' +
                    '                        <button onclick="todoSave('+todos[i].id+')" type="submit">Save</button>\n' +
                    '                    </td>\n' +
                    '                    <td class="smallestCell">\n' +
                    '                        <button onclick="todoDone('+todos[i].id+')" type="submit">Done</button>\n' +
                    '                    </td>\n' +
                    '                    <td class="smallestCell">\n' +
                    '                        <button onclick="todoDestroy('+todos[i].id+')" type="submit">Remove</button>\n' +
                    '                    </td>\n' +
                    '                </tr>';
            }
            html += '</table>';
            return html;
        }

        function generateDonesHtml(todos) {
            if (typeof todos === 'undefined') return "";
            let html = '<table class="todosTable">';
            for(let i = 0; i < todos.length; ++i) {
                html += '<tr>\n' +
                    '                    <td>\n' +
                                            todos[i].content+'\n' +
                    '                    </td>\n' +
                    '                    <td class="smallestCell">\n' +
                                            todos[i].done_date+'\n' +
                    '                    </td>\n' +
                    '                    <td class="smallestCell">\n' +
                    '                        <button onclick="todoUndo('+todos[i].id+')" type="submit">Undo</button>\n' +
                    '                    </td>\n' +
                    '                    <td class="smallestCell">\n' +
                    '                        <button onclick="todoDestroy('+todos[i].id+')" type="submit">Remove</button>\n' +
                    '                    </td>\n' +
                    '                </tr>';
            }
            html += '</table>';
            return html;
        }

        function todoDone(id) {
            let url = '{!! route('todos.done', ':id') !!}';
            url = url.replace(':id', id);
            let xhr = getXMLHttpRequest('GET', url);
            xhr.onload = function() {
                refreshFromResponse(xhr);
            };
            xhr.send();
        }

        function todoUndo(id) {
            let url = '{!! route('todos.undone', ':id') !!}';
            url = url.replace(':id', id);
            let xhr = getXMLHttpRequest('GET', url);
            xhr.onload = function() {
                refreshFromResponse(xhr);
            };
            xhr.send();
        }

        function todoDestroy(id) {
            let url = '{!! route('todos.destroy', ':id') !!}';
            url = url.replace(':id', id);
            let xhr = getXMLHttpRequest('DELETE', url);
            xhr.onload = function() {
                refreshFromResponse(xhr);
            };
            xhr.send(null);
        }

        function todoAdd(content) {
            let url = '{!! route('todos.store') !!}';
            let xhr = getXMLHttpRequest('POST', url);
            xhr.onload = function() {
                refreshFromResponse(xhr);
            };
            xhr.send(encodeURI('content=' + content));
        }

        function todoSave(id) {
            let content = document.getElementById("todoContent"+id).value;
            let url = '{!! route('todos.update', ':id') !!}';
            url = url.replace(':id', id);
            let xhr = getXMLHttpRequest('PUT', url);
            xhr.onload = function() {
                refreshFromResponse(xhr);
            };
            xhr.send(encodeURI('content=' + content));
        }

        function refreshFromResponse(xhr) {
            if (xhr.status === 200) {
                /* Refresh todos*/
                let response = JSON.parse(xhr.responseText);
                document.getElementById("todos").innerHTML = generateTodosHtml(response.todos);
                document.getElementById("dones").innerHTML = generateDonesHtml(response.dones);
            }
            else {
                displayMessage("Sorry, an error occurred. Check your Internet connection and try again.");
                console.error(xhr.responseText);
            }
        }
    </script>

@endsection