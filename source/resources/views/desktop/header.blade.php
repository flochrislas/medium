<div class="headerRow">
        <div class="headerTab" onclick="openPage('{{ route('home') }}')">
                HOME
        </div>
        <div class="headerTab" onclick="openPage('{{ route('websites.index') }}')">
                WEBSITES
        </div>
        <div class="headerTab" onclick="openPage('{{ route('projects.index') }}')">
                PROJECTS
        </div>
        <div class="headerTab" onclick="openPage('{{ route('todos.index') }}')">
                TODOS
        </div>
        <div class="headerTab" onclick="openPage('{{ route('admin.index') }}')">
                ADMIN
        </div>
        <div class="headerTab" onclick="openPage('{{ route('welcome') }}')">
                ROOT
        </div>

        @guest
                <div class="headerTab" onclick="openPage('{{ route('login') }}')">
                        LOGIN
                </div>
                @if (Route::has('register'))
                        <div class="headerTab" onclick="openPage('{{ route('register') }}')">
                                REGISTER
                        </div>
                @endif
        @else
        <div class="headerTab" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                {{ Auth::user()->name }} logout
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                </form>
        </div>
        @endguest
</div>