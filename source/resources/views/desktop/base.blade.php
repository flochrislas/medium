@extends('root')

@push('base-style')
    <link href="/css/desktop.css" rel="stylesheet">
@endpush

@section('base-header')
    @include('desktop.header')
    <div id="message" class="message" onclick="hideMessage()"></div>
@endsection

@section('base-content')
    @yield('content')
@endsection

@section('base-footer')
    {{-- include some footer --}}
@endsection