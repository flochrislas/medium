@extends('desktop.base')

@push('meta')
    <title>{{config('app.name')}} - Test</title>
    <meta name="description" content="Test">
@endpush

@push('style')
    <link href="/css/nodes.css" rel="stylesheet">
    <link href="/css/projects.css" rel="stylesheet">
    <link href="/css/todos.css" rel="stylesheet">
@endpush

@section('content')
    <div class='wrapper'>

        {{-- First column --}}
        @include('lists.nodes')

        {{-- Second column --}}
         @include('lists.projects')

        {{-- Third column --}}
        @include('lists.todos')

    </div>

    <script>
        function newNode() {
            openNewPage("{{ route('nodes.create') }}");
        }
        function loadNode(id) {
            let url = '{!! route('nodes.show', ':id') !!}';
            url = url.replace(':id', id);
            openNewPage(url);
        }
    </script>
@endsection