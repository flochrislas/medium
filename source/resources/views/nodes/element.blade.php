@push('style')
    <link href="{{ asset('css/nodes.css') }}" rel="stylesheet" type="text/css" />
@endpush

@push('script')
    <script src="{{ asset('js/nodes.js') }}" defer></script>
@endpush

<script>
    {{-- Communication between PHP and javascript --}}
    class NodeRoutes {
        static create() {
            return '{!! route('nodes-api.create') !!}';
        }

        static update(id) {
            let url = '{!! route('nodes-api.update', ':id') !!}';
            return url.replace(':id', id);
        }

        static delete(id) {
            let url = '{!! route('nodes-api.delete', ':id') !!}';
            return url.replace(':id', id);
        }

        static readProject(id) {
            let url = '{!! route('nodes-api.read-project', ':id') !!}';
            return url.replace(':id', id);
        }

        static read(id) {
            let url = '{!! route('nodes-api.read', ':id') !!}';
            return url.replace(':id', id);
        }
    }
</script>

<div id="nodeElement" class='flexColumn nodeBorders bgColorNodes'>

    <div class="flexRow">
        <input id="nodeId" type="hidden" value="{{$id ?? ''}}">
        <input id="nodeChangesAreSaved" type="hidden" value="true">
        <input id="nodeTitle" name="nodeInput" type="text" placeholder="Title (required)">
        @include('lists.dropdowns.nodables')
        <input id="nodeNodableType" type="hidden" value="">
        <x-svg-icon id="nodeSaveIcon" class="biggerIcon" icon="save" origin="feather" onclick="saveNode()"/>
        <x-svg-icon id="nodeDeleteIcon" class="biggerIcon none" icon="trash" origin="feather" onclick="showNodeDeleteConfirm()"/>
        <div id="nodeDeleteConfirm" class="none">Confirm: <x-svg-icon id="nodeDeleteConfirmIcon" class="biggerIcon red" icon="trash" origin="feather" onclick="deleteNode()"/></div>
    </div>

    <div class="flexRow">
        <input id="nodeMedia" name="nodeInput" type="text" placeholder="Media (required)">
        <input id="nodeTopic" name="nodeInput" type="text" placeholder="Topic (required)">
        <input id="nodePrivacy" name="nodeInput" type="text" placeholder="Privacy (0:public 1:shareable 2:private 3:secret)">
        <input id="nodeNeedMore" name="nodeInput" type="checkbox" placeholder="need_more">
    </div>

    {{-- Nodable section -------------------------------------------------------------------- --}}
    <div id="nodableBook" class="flexRow nodable book">
        <div>
            <input id="bookTitle" name="nodeInput" type="text" placeholder="Title">
            <input id="bookCreator" name="nodeInput" type="text" placeholder="Author">
            <input id="bookReleaseDate" name="nodeInput" type="text" placeholder="Publication date">
        </div>
        <div>
            <input id="bookStatus" name="nodeInput" type="text" placeholder="Status">
            <input id="bookGrade" name="nodeInput" type="text" placeholder="Grade">
            <input id="bookScore" name="nodeInput" type="text" placeholder="Score">
        </div>
    </div>

    <div id="nodableMovie" class="flexRow nodable movie">
        <div>
            <input id="movieTitle" name="nodeInput" type="text" placeholder="Title">
            <input id="movieCreator" name="nodeInput" type="text" placeholder="Director">
            <input id="movieReleaseDate" name="nodeInput" type="text" placeholder="Release date">
        </div>
        <div>
            <input id="movieStatus" name="nodeInput" type="text" placeholder="Status">
            <input id="movieGrade" name="nodeInput" type="text" placeholder="Grade">
            <input id="movieScore" name="nodeInput" type="text" placeholder="Score">
        </div>
    </div>

    <div id="nodableMusic" class="flexRow nodable music">
        <div>
            <input id="musicCreator" name="nodeInput" type="text" placeholder="Artist">
            <input id="musicReleaseDate" name="nodeInput" type="text" placeholder="Release date">
        </div>
        <div>
            <input id="musicStatus" name="nodeInput" type="text" placeholder="Status">
            <input id="musicGrade" name="nodeInput" type="text" placeholder="Grade">
            <input id="musicScore" name="nodeInput" type="text" placeholder="Score">
        </div>
    </div>

    <div id="nodablePerson" class="flexRow nodable person">
        <input id="personName" name="nodeInput" type="text" placeholder="Name">
        <input id="personContact" name="nodeInput" type="text" placeholder="Contact">
        <input id="personBirthDate" name="nodeInput" type="text" placeholder="Birth date">
    </div>

    <div id="nodableSeries" class="flexRow nodable series">
        <div>
            <input id="seriesTitle" name="nodeInput" type="text" placeholder="Title">
            <input id="seriesReleaseDate" name="nodeInput" type="text" placeholder="Release date">
        </div>
        <div>
            <input id="seriesStatus" name="nodeInput" type="text" placeholder="Status">
            <input id="seriesGrade" name="nodeInput" type="text" placeholder="Grade">
            <input id="seriesScore" name="nodeInput" type="text" placeholder="Score">
        </div>
    </div>

    <div id="nodableVideo" class="flexRow nodable video">
        <div>
            <input id="videoTitle" name="nodeInput" type="text" placeholder="Title">
            <input id="videoCreator" name="nodeInput" type="text" placeholder="Creator">
            <input id="videoReleaseDate" name="nodeInput" type="text" placeholder="Release date">
        </div>
        <div>
            <input id="videoStatus" name="nodeInput" type="text" placeholder="Status">
            <input id="videoGrade" name="nodeInput" type="text" placeholder="Grade">
            <input id="videoScore" name="nodeInput" type="text" placeholder="Score">
        </div>
    </div>

    <div id="nodableVideoGame" class="flexRow nodable videoGame">
        <div>
            <input id="videoGameTitle" name="nodeInput" type="text" placeholder="Title">
            <input id="videoGameCreator" name="nodeInput" type="text" placeholder="Creator">
            <input id="videoGameReleaseDate" name="nodeInput" type="text" placeholder="Release date">
        </div>
        <div>
            <input id="videoGameStatus" name="nodeInput" type="text" placeholder="Status">
            <input id="videoGameGrade" name="nodeInput" type="text" placeholder="Grade">
            <input id="videoGameScore" name="nodeInput" type="text" placeholder="Score">
        </div>
    </div>
    {{-- --------------------------------------------------------------------------------------- --}}

    <div class="flexRow">
        <textarea id="nodeContent" name="nodeInput" class="fullWidthTextArea" rows="10" cols="90" placeholder="Write the content (required)"></textarea>
    </div>

    <div class="flexRow">
        <textarea id="nodeComment" name="nodeInput" class="fullWidthTextArea" rows="3" cols="90" placeholder="Write a comment (optional)"></textarea>
    </div>

    <div class="flexRow">
        <textarea id="nodeTags" name="nodeInput" class="fullWidthTextArea" rows="3" cols="90" placeholder="Input tags, keywords related to this node (optional)"></textarea>
    </div>

</div>
