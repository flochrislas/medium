@extends('desktop.base')

@push('meta')
    <title>{{config('app.name')}} - New Node</title>
    <meta name="description" content="New node">
@endpush

@section('content')
    <div class="singleContent">
        @include('nodes.element')
    </div>
@endsection