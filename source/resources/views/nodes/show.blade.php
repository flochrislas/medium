@extends('desktop.base')

@push('meta')
    <title>{{config('app.name')}} - Node details</title>
    <meta name="description" content="Node details">
@endpush

@section('content')
    <div class="singleContent">
        @include('nodes.element')
    </div>
@endsection