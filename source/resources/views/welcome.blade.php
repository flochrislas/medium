@extends('desktop.base')

@section('metatags')
    <title>{{config('app.name')}} - Welcome</title>
    <meta name="description" content="An annex to your brain, connected to the Internet.">
@endsection

@section('content')
    Welcome, unsuspecting human!
    <br><br>
    Dev pages for:
    <ul>
        <td><a href="{{ route('home') }}">HOME</a></td>
        <li><a href="{{ route('projects.index') }}">Projects</a></li>
        <li><a href="{{ route('notes.index') }}">Notes</a></li>
        <li><a href="{{ route('todos.index') }}">Todos</a></li>
        <li><a href="{{ route('websites.index') }}">Websites</a></li>
        <li><a href="{{ route('nodes.create') }}">New Node</a></li>
        <li><a href="{{ route('test') }}">Test</a></li>
        <li><a href="{{ route('admin.index') }}">Admin page</a></li>
    </ul>
    <br>
    <div>
        By registering to this service, you agree to let all your data be fed to an AI that might one day rule over the world. In accordance to Medium's purpose of destroying the current system of things, Medium pledges to never share nor sell your data to any non AI entity, or any business related entity.
    </div>

    <div>
    <!-- Right Side Of Navbar -->
    <ul class="navbar-nav ml-auto">
        <!-- Authentication Links -->
        @guest
            <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>
            @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
            @endif
        @else
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        @endguest
    </ul>
    </div>

@endsection