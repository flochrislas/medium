<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @if (App::environment('production'))
        @include('inline-javascript.google-analytics')
    @endif
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- viewport for responsive web design -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- SEO -->
    @stack('meta')
    <!-- Style -->
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" /> --}}
    <link href="{{ asset('css/common.css') }}" rel="stylesheet" type="text/css" />
    @stack('base-style')
    @stack('style')
    <!-- Script -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    {{-- <script src="{{ asset('js/app.js') }}" type="text/js"></script> --}}
    <script src="{{ asset('js/common.js') }}" defer></script>
    @stack('script')
</head>
<body>
    <div id="app">
        @yield('base-header')
        @yield('base-content')
        @yield('base-footer')
    </div>
</body>
</html>
