@extends('desktop.base')

@section('metatags')
    <title>{{config('app.name')}} - Projects</title>
    <meta name="description" content="Project view">
@endsection

@push('style')
    <link href="/css/projects.css" rel="stylesheet">
@endpush

@section('content')
    <div class='wrapper'>
        {{-- Project list for selection --}}
        @include('lists.projects')
    </div>
@endsection