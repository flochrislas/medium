<div id ="taskList" class='flexColumn todoBorders bgColorTodos'>
    <div class="listRow header">
        (search) TASKS (add)
    </div>
    @foreach($tasks as $task)
        <div class="listRow">
            {{$task->content}}
        </div>
    @endforeach
</div>