@extends('desktop.base')

@push('meta')
    <title>{{config('app.name')}} - Project</title>
    <meta name="description" content="Project view">
@endpush

@push('style')
    <link href="/css/projects.css" rel="stylesheet">
@endpush

@section('content')
    <div class='wrapper'>

        {{-- Left column --}}
        <div class="flexColumn zero">

            {{-- Project title --}}
            <div class='flexColumn projectBorders bgColorProjects'>
                <div class="listRow">
                    <input id="projectId" type="hidden" value="{{$activeProject->id}}">
                    <div class="rowItem" onclick="openPage('{{ route('projects.index') }}')">
                        @include('icons.chevron-left')
                    </div>
                    <div class="rowItem">
                        {{$activeProject->title}}
                    </div>
                    <div class="rowItem">
                        @include('icons.trash')
                    </div>
                </div>
            </div>

            {{-- Project resource list --}}
            @include('lists.nodes')
        </div>

        {{-- Project node view (main) --}}
        @include('nodes.element')

        {{-- Project tasks --}}
        @include('projects.tasks')

    </div>

@endsection