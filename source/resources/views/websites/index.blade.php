{{-- ------------------------------------------------------------------------------------------
Main page for Speed Dial

TODO: each dial should be a thumbnail of the website's screenshot, taken automatically:
https://www.webslesson.info/2018/06/how-to-take-website-screen-shot-from-url-in-php.html

------------------------------------------------------------------------------------------ --}}

@extends('desktop.base')

@push('meta')
    <title>{{config('app.name')}} - Websites</title>
    <meta name="description" content="Quick access to your favorite websites.">
@endpush

@push('style')
    <link href="/css/websites.css" rel="stylesheet">
@endpush

@push('script')
    <script src="{{ asset('js/websites.js') }}" defer></script>
@endpush

@include('inline-javascript.svg-icons')
@include('inline-javascript.websites-routes')

@section('content')

    <div class="flexColumnsContainer">

        @include('lists.websiteGroups')

        <div class="flexContainer" id="websites">
            @foreach($websites as $website)
                <div class="websiteElement" onclick="openWebsite('{{$website->url}}', {{$website->id}})">
                    {{$website->title}}
                    <input class="websiteEditTitle" type="text" id="websiteEditTitle{{$website->id}}" value="{{$website->title}}" onclick="stopPropagation()">
                    <input class="websiteEditUrl" type="text" id="websiteEditUrl{{$website->id}}" value="{{$website->url}}" onclick="stopPropagation()">
                    <div class="websiteEdit" onclick="editWebsite({{$website->id}})"></div>
                    <div class="websiteRemove" onclick="deleteWebsite({{$website->id}})"></div>
                </div>
            @endforeach
            <div id="websiteEditNew" class="websiteElement none">
                New website:
                <input class="websiteEditTitle" type="text" id="websiteEditTitleNew" value="" placeholder="Title">
                <input class="websiteEditUrl" type="text" id="websiteEditUrlNew" value="" placeholder="URL">
                <div class="websiteEdit" onclick="createWebsite()"></div>
                <div class="websiteRemove" onclick="resetWebsiteCreationForm()"></div>
            </div>
            <div id="websiteAdd" class="websiteElement add" onclick="showWebsiteCreationForm()">
                @include('icons.plus')
            </div>
            <input type="hidden" id="activeWebsiteGroupId" value="1">
        </div>
    </div>

@endsection