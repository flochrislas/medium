<div id="projectsList" class='flexColumn projectBorders bgColorProjects'>
    <div class="listRow header">
        <div id="projectSearchButton" class="projectSearchButton" onclick="toggleProjectSearch()">@include('icons.search')</div>
        <div id="projectHeader" class="projectHeader">PROJECTS</div>
        <input id="projectSearch" class="projectSearch" type="text" onkeyup="filterProjects()">
        <x-svg-icon icon="x" origin="feather" id="projectSearchClose" class="projectSearchClose" onclick="closeProjectSearch()"/>
        <div id="projectAddButton" class="projectAddButton" onclick="newProject()">@include('icons.plus-square')</div>
    </div>
    @foreach($projects as $project)
        <div class="listRow" onclick="openPage('{{ route('projects.show', $project->id) }}')">
            {{$project->title}}
        </div>
    @endforeach
</div>

<script>
    /** TODO: this doesnt work, so redo it based on websiteGroups **/
    function generateProjectListHtml(projectList) {
        if (typeof projectList === 'undefined') return "";
        let html = '';
        for (let i = 0; i < projectList.length; ++i) {
            html += '' +
                '<div class="projectRow">\n' +
                '   <div id="project'+projectList[i].id+'Title" class="projectTitle" onclick="loadProject('+projectList[i].id+')">'+projectList[i].title+'</div>\n' +
                '</div>'
        }
        document.getElementById("projectListContent").innerHTML = html;
    }

    function toggleProjectSearch() {
        // Hide header icons
        document.getElementById("projectSearchButton").style.display = "none";
        document.getElementById("projectHeader").style.display = "none";
        document.getElementById("projectAddButton").style.display = "none";
        // Show filter inputs
        document.getElementById("projectSearch").style.display = "inline-block";
        document.getElementById("projectSearchClose").style.display = "inline-block";
        // Focus on search text box
        document.getElementById("projectSearch").focus();
        document.getElementById("projectSearch").select();
    }

    function closeProjectSearch() {
        // Show header icons
        document.getElementById("projectSearchButton").style.display = "inline-block";
        document.getElementById("projectHeader").style.display = "inline-block";
        document.getElementById("projectAddButton").style.display = "inline-block";
        // Hide filter inputs
        document.getElementById("projectSearch").style.display = "none";
        document.getElementById("projectSearchClose").style.display = "none";
        // Make all rows visible
        let rows = document.getElementsByClassName('projectRow');
        for (let i = 0; i < rows.length; ++i) {
            rows[i].style.display = "";
        }
    }

    function filterProjects() {
        let input, filter, rows, i, title, txtValue;
        input = document.getElementById('projectSearch');
        filter = input.value.toUpperCase();
        rows = document.getElementsByClassName('projectRow');

        // Loop through all list items, and hide those who don't match the search query
        for (i = 0; i < rows.length; ++i) {
            title = rows[i].getElementsByClassName('projectTitle')[0];
            txtValue = title.textContent || title.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                rows[i].style.display = "";
            } else {
                rows[i].style.display = "none";
            }
        }
    }
</script>