<style>
    .nodeButton {
        background-color: var(--color-nodes);
        /* top | right | bottom | left */
        padding: 0.5rem 0.7rem 0.5rem 0.7rem;
        font-size: 1rem;
        border-radius: var(--radius-input);
        border: none;
        cursor: pointer;
    }
    .nodeButton:hover, .nodeButton:focus {
        background-color: var(--color-nodes-02);
    }

    #nodableSearch {
        box-sizing: border-box;
        background-image: url('searchicon.png'); /* TODO: get that icon*/
        background-position: 14px 12px;
        background-repeat: no-repeat;
        font-size: 1rem;
        /* padding: 14px 20px 12px 45px; */
        padding: 0.8rem 1.2rem 0.7rem 2rem;
        border: none;
        border-bottom: 1px solid var(--color-nodes);
        border-radius: var(--radius-input);
    }
    #nodableSearch:focus {outline: 3px solid var(--color-nodes);}

    .dropdown {
        position: relative;
        display: inline-block;
    }

    .dropdown-content {
        display: none;
        position: absolute;
        background-color: var(--bg-color-input);
        min-width: 5rem;
        overflow: auto;
        border: 1px solid var(--color-nodes);
        border-radius: var(--radius-input);
        z-index: 1;
    }

    .nodableRow {
        padding: 0.6rem 1rem;
        text-decoration: none;
        display: block;
    }
    .nodableRow:hover {background-color: var(--bg-color-04);}

</style>

<div class="dropdown">
    <button onclick="toggleNodableDropdown()" class="nodeButton">Customize Type</button>
    <div id="nodableDropdown" class="dropdown-content">
        <input type="text" placeholder="Search.." id="nodableSearch" onkeyup="filterNodableList()">
        @foreach($nodables as $nodable)
            <div class="nodableRow" onclick="selectNodableType('{{$nodable->model}}')">{{$nodable->label}}</div>
        @endforeach
    </div>
</div>

<script>

    function selectNodableType(nodableType) {
        toggleNodableDropdown();
        setNodableType(nodableType);
    }

    /* When the user clicks on the button,
    toggle between hiding and showing the dropdown content */
    function toggleNodableDropdown() {
        document.getElementById("nodableDropdown").classList.toggle("block");
    }

    function filterNodableList() {
        let input, filter, rows, i, label, txtValue;
        input = document.getElementById('nodableSearch');
        filter = input.value.toUpperCase();
        rows = document.getElementsByClassName('nodableRow');

        // Loop through all list items, and hide those who don't match the search query
        for (i = 0; i < rows.length; ++i) {
            label = rows[i];
            txtValue = label.textContent || label.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                rows[i].style.display = "";
            } else {
                rows[i].style.display = "none";
            }
        }
    }

</script>