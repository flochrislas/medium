@push('script')
    <script src="{{ asset('js/websiteGroups.js') }}" defer></script>
@endpush

@include('inline-javascript.website-groups-routes')

<div id="websiteGroupList" class="flexColumn">
    <div class="listRow header oneLine">
        <div id="websiteGroupToggleMoveButton" class="websiteGroupToggleMoveButton" onclick="toggleWebsiteGroupSwap()">@include('icons.swap')<input type="hidden" value="0" id="showWebsiteGroupSwap"></div>
        <div id="websiteGroupSearchButton" class="websiteGroupSearchButton" onclick="toggleWebsiteGroupSearch()">@include('icons.search')</div>
        <div id="websiteGroupHeader" class="websiteGroupHeader">GROUPS</div>
        <input id="websiteGroupSearch" class="websiteGroupSearch" type="text" onkeyup="filterWebsiteGroups()">
        <x-svg-icon icon="x" origin="feather" id="websiteGroupSearchClose" class="websiteGroupSearchClose" onclick="closeWebsiteGroupSearch()"/>
        <div id="websiteGroupAddButton" class="websiteGroupAddButton" onclick="storeWebsiteGroup('edit me!')">@include('icons.plus-square')</div>
    </div>
    <div id="websiteGroupListContent" class="flexColumn zero">
    @foreach($websiteGroups as $websiteGroup)
        <div id="websiteGroupRow{{$websiteGroup->id}}" class="websiteGroupRow">
            <div id="websiteGroupEditButton{{$websiteGroup->id}}" class="websiteGroupEditButton" onclick="editWebsiteGroup({{$websiteGroup->id}})">@include('icons.edit')</div>
            @if (!$loop->first) <div class="websiteGroupMove" onclick="moveWebsiteGroup({{$websiteGroup->id}}, {{$websiteGroup->position}}-1)">@include('icons.swap-up')</div> @endif
            @if (!$loop->last) <div class="websiteGroupMove" onclick="moveWebsiteGroup({{$websiteGroup->id}}, {{$websiteGroup->position}}+1)">@include('icons.swap-down')</div> @endif
            <div id="websiteGroupTitle{{$websiteGroup->id}}" class="websiteGroupTitle" contenteditable="false" onclick="websiteShowFromGroup({{$websiteGroup->id}})">{{$websiteGroup->title}}</div>
            <div id="websiteGroupUpdateButton{{$websiteGroup->id}}" class="websiteGroupUpdateButton" onclick="updateWebsiteGroup({{$websiteGroup->id}})">@include('icons.save')</div>
            <div id="websiteGroupRemoveButton{{$websiteGroup->id}}" class="websiteGroupRemoveButton" onclick="destroyWebsiteGroup({{$websiteGroup->id}})">@include('icons.trash')</div>
        </div>
    @endforeach
    </div>
</div>