<div id="nodeList" class='flexColumn nodeBorders bgColorNodes'>
    <div class="listRow header">
        <div id="nodeSearchButton" class="nodeSearchButton" onclick="toggleNodeSearch()">@include('icons.search')</div>
        <div id="nodeHeader" class="nodeHeader">NODES</div>
        <input id="nodeSearch" class="nodeSearch" type="text" onkeyup="filterNodes()">
        <x-svg-icon icon="x" origin="feather" id="nodeSearchClose" class="nodeSearchClose" onclick="closeNodeSearch()"/>
        <div id="nodeAddButton" class="nodeAddButton" onclick="newNode()">@include('icons.plus-square')</div>
    </div>
    <div id="nodeListContent" class="flexColumn zero">
    @foreach($nodes as $node)
        <div class="nodeRow">
            <div id="node{{$node->id}}Title" class="nodeTitle" onclick="loadNode({{$node->id}})">{{$node->title}}</div>
        </div>
    @endforeach
    </div>
</div>

<script>
    function generateNodeListHtml(nodeList) {
        if (typeof nodeList === 'undefined') return "";
        let html = '';
        for (let i = 0; i < nodeList.length; ++i) {
            html += '' +
                '<div class="nodeRow">\n' +
                '   <div id="node'+nodeList[i].id+'Title" class="nodeTitle" onclick="loadNode('+nodeList[i].id+')">'+nodeList[i].title+'</div>\n' +
                '</div>'
        }
        document.getElementById("nodeListContent").innerHTML = html;
    }

    function toggleNodeSearch() {
        // Hide header icons
        document.getElementById("nodeSearchButton").style.display = "none";
        document.getElementById("nodeHeader").style.display = "none";
        document.getElementById("nodeAddButton").style.display = "none";
        // Show filter inputs
        document.getElementById("nodeSearch").style.display = "inline-block";
        document.getElementById("nodeSearchClose").style.display = "inline-block";
        // Focus on search text box
        document.getElementById("nodeSearch").focus();
        document.getElementById("nodeSearch").select();
    }

    function closeNodeSearch() {
        // Show header icons
        document.getElementById("nodeSearchButton").style.display = "inline-block";
        document.getElementById("nodeHeader").style.display = "inline-block";
        document.getElementById("nodeAddButton").style.display = "inline-block";
        // Hide filter inputs
        document.getElementById("nodeSearch").style.display = "none";
        document.getElementById("nodeSearchClose").style.display = "none";
        // Make all rows visible
        setDisplayByClassName("nodeRow", "");
    }

    function filterNodes() {
        let input, filter, rows, i, title, txtValue;
        input = document.getElementById('nodeSearch');
        filter = input.value.toUpperCase();
        rows = document.getElementsByClassName('nodeRow');

        // Loop through all list items, and hide those who don't match the search query
        for (i = 0; i < rows.length; ++i) {
            title = rows[i].getElementsByClassName('nodeTitle')[0];
            txtValue = title.textContent || title.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                rows[i].style.display = "";
            } else {
                rows[i].style.display = "none";
            }
        }
    }

</script>