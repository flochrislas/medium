<script>
    {{-- Communication between PHP and Javascript --}}
    class WebsiteGroupRoutes {
        static create() {
            return '{!! route('websiteGroups-api.create') !!}';
        }

        static update(id) {
            let url = '{!! route('websiteGroups-api.update', ':id') !!}';
            return url.replace(':id', id);
        }

        static delete(id) {
            let url = '{!! route('websiteGroups-api.delete', ':id') !!}';
            return url.replace(':id', id);
        }

        static position() {
            return '{!! route('websiteGroups-api.position') !!}';
        }
    }
</script>