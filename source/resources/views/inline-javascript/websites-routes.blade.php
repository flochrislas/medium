<script>
    {{-- Communication between PHP and Javascript --}}
    class WebsiteRoutes {
        static create() {
            return '{!! route('websites-api.create') !!}';
        }

        static update(id) {
            let url = '{!! route('websites-api.update', ':id') !!}';
            return url.replace(':id', id);
        }

        static delete(id) {
            let url = '{!! route('websites-api.delete', ':id') !!}';
            return url.replace(':id', id);
        }

        static readGroup(id) {
            let url = '{!! route('websites-api.read-group', ':id') !!}';
            return url.replace(':id', id);
        }
    }
</script>