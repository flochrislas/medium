<script>
    {{-- This allows to write SVG icons from Javascript files --}}
    class SvgIcons {
        static plus() {
            return '@include('icons.plus')';
        }
        static swap() {
            return '@include('icons.swap')';
        }
        static search() {
            return '@include('icons.search')';
        }
        static plusSquare() {
            return '@include('icons.plus-square')';
        }
        static edit() {
            return '@include('icons.edit')';
        }
        static swapUp() {
            return '@include('icons.swap-up')';
        }
        static swapDown() {
            return '@include('icons.swap-down')';
        }
        static save() {
            return '@include('icons.save')';
        }
        static trash() {
            return '@include('icons.trash')';
        }
    }
</script>