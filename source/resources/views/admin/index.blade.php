@extends('desktop.base')

@section('metatags')
    <title>{{config('app.name')}} - Admin</title>
    <meta name="description" content="Admin page">
@endsection

@section('content')
    <div>
        <button onclick="runDbDump()" type="submit">Run DB dump</button>
        <br><br>
        <button onclick="runDbDumpTables()" type="submit">Run DB tables dump</button>
        <br><br>
        <button onclick="clearCache()" type="submit">Clear server caches</button>
        <br><br>
        <button onclick="clearBackups()" type="submit">Delete backups files</button>
        <br><br>
        <div id="commandHistory"></div>
        <br><br>
    </div>

    <script>

        function clearBackups() {
            let url = '{!! route('admin.clearBackups') !!}';
            let xhr = getXMLHttpRequest('POST', url);
            xhr.onload = function() {
                refreshCommandHistory(xhr);
            };
            xhr.send();
        }

        function clearCache() {
            let url = '{!! route('admin.clearCache') !!}';
            let xhr = getXMLHttpRequest('POST', url);
            xhr.onload = function() {
                refreshCommandHistory(xhr);
            };
            xhr.send();
        }

        function runDbDump() {
            let url = '{!! route('admin.dumpDb') !!}';
            let xhr = getXMLHttpRequest('POST', url);
            xhr.onload = function() {
                refreshCommandHistory(xhr);
            };
            xhr.send();
        }

        function runDbDumpTables() {
            let url = '{!! route('admin.dumpDbTables') !!}';
            let xhr = getXMLHttpRequest('POST', url);
            xhr.onload = function() {
                refreshCommandHistory(xhr);
            };
            xhr.send();
        }

        function refreshCommandHistory(xhr) {
            if (xhr.status === 200 || xhr.status === 201) {
                let response = JSON.parse(xhr.responseText);
                document.getElementById("commandHistory").innerHTML += "<br>"+response.message;
            } else if (xhr.status === 500) {
                document.getElementById("commandHistory").innerHTML += "<br>"+"ERROR: "+xhr.responseText;
            } else {
                document.getElementById("commandHistory").innerHTML += "<br>"+xhr.responseText;
            }
        }

    </script>
@endsection