@extends('root')

@section('metatags')
    @yield('metatags')
@endsection

@section('style')
    <link href="/css/mobile.css" rel="stylesheet">
@endsection

@section('header')
    <!-- include some header -->
@endsection

@section('content')
    @yield('content')
@endsection

@section('footer')
    <!-- include some footer -->
@endsection