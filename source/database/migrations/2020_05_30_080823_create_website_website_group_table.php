<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Pivot table for website groups and their websites
 *
 * Class CreateWebsiteWebsiteGroupTable
 */
class CreateWebsiteWebsiteGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // We use on cascade delete so if any entry is deleted,
        // its relationships will as well, and we dont need to manually detach from code
        Schema::create('website_website_group', function (Blueprint $table) {
            $table->unsignedBigInteger('website_id');
            $table->foreign('website_id')->references('id')
                ->on('websites')->onDelete('cascade');

            $table->unsignedBigInteger('website_group_id');
            $table->foreign('website_group_id')->references('id')
                ->on('website_groups')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('website_website_group');
    }
}
