<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('title')->nullable()->comment('The title of the book. Suggestion: "The shock doctrine (en), La stratégie du shock (fr)"');
            $table->string('creator')->nullable()->comment('The author(s) of the book. Multiple authors would be separated by a comma.');
            $table->date('release_date')->nullable()->comment('When the book was (first) published.');
            $table->smallInteger('status')->default('0')->comment('0: unread, 1:read(finished), 2:unfinished');
            $table->smallInteger('grade')->nullable()->comment('0:mediocre, 1:may contain some potentially interesting thing(s), 2.idk... 3:good, 4:brilliant');
            $table->integer('score')->nullable()->comment('Could be anything, this is for relative positioning between books.');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
