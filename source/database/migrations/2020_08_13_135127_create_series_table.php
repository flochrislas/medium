<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('series', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('title')->nullable()->comment('The title. Suggestion: "Original title (en), French title (fr)"');
            $table->date('release_date')->nullable()->comment('When the it was (first) released.');
            $table->smallInteger('status')->default('0')->comment('0:unwatched, 1:watched(finished), 2:unfinished');
            $table->smallInteger('grade')->nullable()->comment('0:mediocre, 1:may contain some potentially interesting thing(s), 2:idk..., 3:good, 4:brilliant');
            $table->integer('score')->nullable()->comment('Could be anything, this is for relative positioning in the list.');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('series');
    }
}
