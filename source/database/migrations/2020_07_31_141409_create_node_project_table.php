<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateNodeProjectTable
 * Pivot table for n-n relation between node and project.
 * Table name must be singular and model in ALPHABETICAL ORDER
 */
class CreateNodeProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('node_project', function (Blueprint $table) {
            $table->unsignedBigInteger('node_id');
            $table->foreign('node_id')->references('id')
                ->on('nodes');

            $table->unsignedBigInteger('project_id');
            $table->foreign('project_id')->references('id')
                ->on('projects');
        });
        // Note: the foreign key constraint is not necessary for this to work
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('node_project');
    }
}
