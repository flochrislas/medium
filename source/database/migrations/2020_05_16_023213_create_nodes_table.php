<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nodes', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('media')->comment('Ex: website, youtube, note, image, pdf, audio, file...'); // TODO: a foreign table
            $table->text('tags')->nullable()->comment('Keywords that will allow to search and organize in many ways');  // TODO: add a n-n tags (ntags)
            $table->string('topic')->comment('Acts as a main tag, ex: movie, book, videogame, music, history, politics, idea...');
            $table->text('content')->comment('If it is is a linked content, just the URL. If it is a note, then lots of text');
            $table->text('comment')->nullable()->comment('Info about the content (especially if it is just a link), that could be parsed for auto-tags');
            $table->string('title')->comment('Acts as a human readable ID, get in a sec what node is it');
            $table->smallInteger('privacy')->default(0)->comment('Default 0:public, 1:shareable(user,group,circles), 2:private, 3:secret(encrypted content)');
            $table->boolean('need_more')->default(0)->comment('Helpful flag for when we want to come back to study or add more info to the node, 0: no intend to do more on this node soon, 1: want to do more on this node soon');
            $table->string('nodable_type')->nullable()->comment('For example, "book", for the book (nodable) extension that will add "author" fields etc...'); // TODO: a foreign table extensions with label and table name
            $table->bigInteger('nodable_id')->nullable()->comment('The id that matches one to one with the (nodable) extension tables, for example, if extension_type is "book", this would be the id to lookup in the "books" table in order to get the additional fields');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nodes');
    }
}
