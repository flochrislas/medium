<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTodosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('todos', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('content');
            $table->timestamp('done_date')->nullable();
            $table->smallInteger('priority')->nullable();
            $table->smallInteger('difficulty')->nullable();
            $table->smallInteger('weight')->nullable();
            $table->smallInteger('category')->nullable();
            $table->smallInteger('project')->nullable();
            // $table->foreignId('project_id')->constrained(); // doesnt work? removed until it becomes necessary
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('todos');
    }
}
