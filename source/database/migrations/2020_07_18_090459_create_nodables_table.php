<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNodablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nodables', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('label')->comment('Label that will be used in the UI.');
            $table->string('model')->comment('Class name of the model in the implementation.');
            $table->string('table')->comment('Table name containing the data in the database.');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nodables');
    }
}
