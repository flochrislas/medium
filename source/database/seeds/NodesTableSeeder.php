<?php

use Illuminate\Database\Seeder;
use App\Node;
use App\Nodables\Website;

class NodesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Clear
        // DB::table('nodes')->delete();

        // Fill
        $node = Node::create(array(
            'title'             => 'Buying an office chair',
            'content'           => 'It seems the Aeron chair is really great.',
            'media'             => 'note',
            'tags'              => 'chair, office chair, purchase, buy',
            'topic'             => 'chair',
            'comment'           => 'This is a note of investigation in order to buy a new office chair for my desk at home.',
            'privacy'           => 0,
            'need_more'          => 0
        ));


        $node = Node::create(array(
            'title'             => 'Amazon Japan',
            'content'           => 'https://www.amazon.co.jp/',
            'media'             => 'website',
            'tags'              => 'shopping',
            'topic'             => 'shopping',
            'comment'           => 'Huge marketplace, fast deliveries.',
            'privacy'           => 0,
            'need_more'          => 0
        ));
        $website = Website::create(array(
            'title'             => 'Amazon Japan',
            'url'               => 'https://www.amazon.co.jp/',
            'thumbnail'         => ''
        ));
        $website->attachWebsiteGroups(['2']);
        $website->node()->save($node);

        $node = Node::create(array(
            'title'             => 'Google Japan',
            'content'           => 'https://www.google.co.jp/',
            'media'             => 'website',
            'tags'              => 'search',
            'topic'             => 'search',
            'comment'           => 'Biggest web search engine',
            'privacy'           => 0,
            'need_more'          => 0
        ));
        $website = Website::create(array(
            'title'             => 'Google Japan',
            'url'               => 'https://www.google.co.jp/',
            'thumbnail'         => ''
        ));
        $website->attachWebsiteGroups(['1']);
        $website->node()->save($node);

    }
}
