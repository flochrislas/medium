<?php

use Illuminate\Database\Seeder;
use App\WebsiteGroup;

class WebsiteGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Clear
        // DB::table('website_groups')->delete();

        // Fill
        WebsiteGroup::create(array(
            'title'             => 'Daily websites',
            'position'          => 1
        ));

        WebsiteGroup::create(array(
            'title'             => 'Shopping',
            'position'          => 2
        ));

        WebsiteGroup::create(array(
            'title'             => 'Download',
            'position'          => 3
        ));
    }
}
