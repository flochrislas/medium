<?php

use Illuminate\Database\Seeder;

/**
 * Register seeders here in order to run them
 */
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ProjectsTableSeeder::class);
        $this->call(TodosTableSeeder::class);
        $this->call(NodablesTableSeeder::class);
        $this->call(WebsitesTableSeeder::class);
        $this->call(WebsiteGroupsTableSeeder::class);
        $this->call(NodesTableSeeder::class);

        // Clear the cache
        \Cache::flush();
    }
}
