<?php

use Illuminate\Database\Seeder;
use App\Nodable;

class NodablesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Fill
        $nodable = Nodable::create(array(
            'label'             => 'Note',
            'model'             => 'Note',
            'table'             => 'notes'
        ));

        $nodable = Nodable::create(array(
            'label'             => 'Book',
            'model'             => 'Book',
            'table'             => 'books'
        ));

        $nodable = Nodable::create(array(
            'label'             => 'Movie',
            'model'             => 'Movie',
            'table'             => 'movies'
        ));

        $nodable = Nodable::create(array(
            'label'             => 'Series',
            'model'             => 'Series',
            'table'             => 'series'
        ));

        $nodable = Nodable::create(array(
            'label'             => 'Video',
            'model'             => 'Video',
            'table'             => 'videos'
        ));

        $nodable = Nodable::create(array(
            'label'             => 'Music',
            'model'             => 'Music',
            'table'             => 'music'
        ));

        $nodable = Nodable::create(array(
            'label'             => 'Video Game',
            'model'             => 'VideoGame',
            'table'             => 'video_games'
        ));

        $nodable = Nodable::create(array(
            'label'             => 'Website',
            'model'             => 'Website',
            'table'             => 'websites'
        ));

        $nodable = Nodable::create(array(
            'label'             => 'Person',
            'model'             => 'Person',
            'table'             => 'people'
        ));
    }
}
