<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Fill
        User::create(array(
            'name'            => 'Mr. Test',
            'email'           => 'aze@aze.aze',
            'password'        => '$2y$10$Rwg9WJJalH77CmQCPvoP3.tsOQ01okiMYK2wb64ud4B7fKs4brKLq' // 'azeazeaze'
        ));
    }
}
