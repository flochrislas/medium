<?php

use Illuminate\Database\Seeder;
use App\Project;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Clear
        // DB::table('projects')->delete();

        // Fill
        Project::create(array(
            'title'             => 'Project 1'
        ));

        Project::create(array(
            'title'             => 'Project 2'
        ));

        Project::create(array(
            'title'             => 'Project 3'
        ));
    }
}
