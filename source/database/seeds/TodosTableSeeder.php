<?php

use Illuminate\Database\Seeder;
use App\Todo;

class TodosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Clear
        // DB::table('todos')->delete();

        // Fill
        Todo::create(array(
            'content'              => 'todo 1',
            'priority'             => '1',
            'difficulty'           => '1',
            'weight'               => '1',
            'category'             => '1',
            'project'              => '1',
            'done_date'            => null
        ));

        Todo::create(array(
            'content'              => 'todo 2',
            'priority'             => '2',
            'difficulty'           => '2',
            'weight'               => '2',
            'category'             => '2',
            'project'              => '2',
            'done_date'            => null
        ));

        Todo::create(array(
            'content'              => 'todo 3',
            'priority'             => '3',
            'difficulty'           => '3',
            'weight'               => '3',
            'category'             => '3',
            'project'              => '3',
            'done_date'            => null
        ));

        Todo::create(array(
            'content'              => 'done 1',
            'priority'             => '1',
            'difficulty'           => '1',
            'weight'               => '1',
            'category'             => '1',
            'project'              => '1',
            'done_date'            => '2019-11-11 11:11:11'
        ));

        Todo::create(array(
            'content'              => 'done 2',
            'priority'             => '2',
            'difficulty'           => '2',
            'weight'               => '2',
            'category'             => '2',
            'project'              => '2',
            'done_date'            => '2019-11-11 11:11:11'
        ));
    }
}
