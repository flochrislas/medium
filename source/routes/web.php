<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 * List all routes and info with "php artisan route:list" or "php artisan route:list" <-------
 * https://stackoverflow.com/questions/25290229/laravel-named-route-for-resource-controller
 */


/* root route */
Route::get('/', function () {
    return view('welcome');
})->name('welcome');

/* to test stuff really quick... */
Route::get('/test', function () {
    return view('tests.index');
})->name('test');


/*
 * Auto generated routes for login, register, password
 */
Auth::routes();


/*
 * GROUP all the routes that requires authentication
 */
Route::middleware(['auth'])->group(function () {

    /* Start of an idea to control usage of mobile vs desktop design, using a Controller */
    Route::get('home', 'PageController@home')->name('home');

    /*
     * Admin stuff
     */
    Route::get('admin', 'AdminController@index')->name('admin.index');
    Route::post('admin/dumpDb', 'AdminController@runDbDump')->name('admin.dumpDb');
    Route::post('admin/dumpDbTables', 'AdminController@runDbTablesDump')->name('admin.dumpDbTables');
    Route::post('admin/clearCache', 'AdminController@clearCache')->name('admin.clearCache');
    //Route::get('admin/downloadFile/{path}', 'AdminController@downloadFile')->where('path', '.*')->name('admin.downloadFile');
    Route::get('admin/downloadFile', 'AdminController@downloadFile')->name('admin.downloadFile');
    Route::post('admin/clearBackups', 'AdminController@clearBackups')->name('admin.clearBackups');

    /*
     * nodes
     */
    Route::get('nodes/create', 'NodeController@create')->name('nodes.create');
    Route::get('nodes/{id}', 'NodeController@show')->name('nodes.show');


    /*
     * to-do list
     */
    Route::resource('todos', 'TodoController');
    Route::get('todos/done/{todo}', 'TodoController@done')->name('todos.done');
    Route::get('todos/undone/{todo}', 'TodoController@undone')->name('todos.undone');

    /*
     * websites
     */
    // ui endpoint for main page
    Route::get('websites', 'WebsiteController@index')->name('websites.index');
    // json api used in ajax todo: move to api routes?
    Route::post('websites', 'WebsiteApiController@create')->name('websites-api.create');
    Route::put('websites/{id}', 'WebsiteApiController@update')->name('websites-api.update');
    Route::delete('websites/{id}', 'WebsiteApiController@delete')->name('websites-api.delete');
    Route::get('websites/group/{group}', 'WebsiteApiController@group')->name('websites-api.read-group');

    /*
     * websiteGroups
     */
    Route::post('websiteGroups', 'WebsiteGroupController@store')->name('websiteGroups-api.create');
    Route::post('websiteGroups/position', 'WebsiteGroupController@updatePosition')->name('websiteGroups-api.position');
    Route::put('websiteGroups/{websiteGroup}', 'WebsiteGroupController@update')->name('websiteGroups-api.update');
    Route::delete('websiteGroups/{websiteGroup}', 'WebsiteGroupController@destroy')->name('websiteGroups-api.delete');

    /*
     * projects
     */
    Route::get('projects/{project}', 'ProjectController@show')->name('projects.show');
    Route::get('projects', 'ProjectController@index')->name('projects.index');

    /*
     * notes
     */
    Route::resource('notes', 'NoteController');
});
