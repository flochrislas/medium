<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
 * List all routes and info with "php artisan route:list" or "php artisan route:list" <-----
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*
 * Nodes
 */
Route::post('nodes', 'NodeApiController@create')->name('nodes-api.create');
Route::get('nodes/{id}', 'NodeApiController@read')->name('nodes-api.read');
Route::get('nodes/project/{id}', 'NodeApiController@readForProject')->name('nodes-api.read-project');
Route::put('nodes/{id}', 'NodeApiController@update')->name('nodes-api.update');
Route::delete('nodes/{id}', 'NodeApiController@delete')->name('nodes-api.delete');

/*
 * Nodables
 */
Route::get('nodables', 'NodableApiController@readAll')->name('nodables-api.read-all');
